module.exports = function(grunt) {
    grunt.initConfig({

        less: {
            dashboard: {
                options: {
                    paths: ["assets/css"]
                },
                files: {"public/css/skins/custom.css": "public/css/custom.less"}
            },
            website: {
                options: {
                    paths: ["assets/css"]
                },
                files: {"public/css/skins/website.css": "public/css/website.less"}
            },
            production: {
                options: {
                    paths: ["assets/css"],
                    cleancss: true
                },
                files: {"path/to/result.css": "path/to/source.less"}
            }
        },
        watch: {
            files: "public/css/*",
            tasks: ["less:dashboard", "less:website"]
        }
    });
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.registerTask('default', ['watch']);
};