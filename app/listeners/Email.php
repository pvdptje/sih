<?php



namespace App\Listeners;


use App\Eventing\EventListener;

use App\Signup\ApplicationSucceeded;
use App\Website\Checkout\OrderIsPlaced;


class Email extends EventListener {


    public function whenApplicationSucceeded( ApplicationSucceeded $event )
    {

        $application = $event->getApplication();


        $this->app['mailer']->send('emails.signup.thanks', ['application' => $application], function( $message ) use ($application){

            $message->to('patrickvanderpols@live.nl', 'Naam test')
                    ->subject('Bedankt voor uw aanmelding');

        });

        echo 'mail gestuurd ;)';

    }


    public function whenOrderIsPlaced( OrderIsPlaced $event )
    {

        print_r($event);
        exit;


    }


}