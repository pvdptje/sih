<?php

use Illuminate\Database\Migrations\Migration;

class CreateFoodcategoriesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_categories', function($table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('restaurants_id')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('food_categories');
    }

}