<?php

use Illuminate\Database\Migrations\Migration;

class CreateDeliveryzonesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_zones', function($table) {
            $table->increments('id');
            $table->integer('restaurant_id')->index();
            $table->string('zipcode', 6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('delivery_zones');
    }

}