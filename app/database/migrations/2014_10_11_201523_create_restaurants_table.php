<?php

use Illuminate\Database\Migrations\Migration;

class CreateRestaurantsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function($table) {
            $table->increments('id');
            $table->string('subdomain', 255);
            $table->string('name', 255);
            $table->string('adress', 255);
            $table->string('zipcode', 6);
            $table->string('phonenumber', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('restaurants');
    }

}