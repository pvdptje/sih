<?php


use Faker\Factory;

class RestaurantSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $faker = Factory::create('nl_NL');



        foreach(range(43,62) as $i)
        {

            $moderator = \App\Dashboard\Moderator\Moderator::create([
                'username' => $faker->userName,
                'lastname' => $faker->lastName,
                'firstname' => $faker->firstName,
                'password' => Hash::make('dddcom21'),
                'email' => $faker->email,
            ]);


            $restaurant = \App\Restaurant\Restaurant::findOrFail($i);
            $restaurant->moderators()->attach($moderator->id);

            $restaurant->save();



        }
    }

}
