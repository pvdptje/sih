<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/20/14
 * Time: 11:06 PM
 */

namespace App\Restaurant;


use Illuminate\Database\Eloquent\Model;

class Extra extends Model{


    public $table = 'extras';

    public $timestamps = false;


    public function products()
    {
        return $this->belongsToMany('App\\Restaurant\\Product', 'products_extras', 'extras_id', 'products_id');
    }

    public function values()
    {
        return $this->hasMany('App\\Restaurant\\ExtraValue', 'extras_id');
    }

} 