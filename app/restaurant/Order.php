<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 11/9/14
 * Time: 2:26 PM
 */



namespace App\Restaurant;


use App\Eventing\EventGenerator;
use App\Website\Checkout\OrderIsPlaced;
use Gloudemans\Shoppingcart\Cart;
use Illuminate\Database\Eloquent\Model;

class Order extends  Model {


    use EventGenerator;

    public static function createNew(Restaurant $restaurant, Cart $cart, $input = array())
    {
        $order = new self();
        $order->firstname = $input['firstname'];
        $order->lastname = $input['lastname'];
        $order->address = $input['street'] . ' ' . $input['housenumber'];
        $order->city = $input['city'];
        $order->postcode = $input['postcode'];
        $order->email = $input['email'];
        $order->delivery_time = 'z.s.m';
        $order->products = serialize($cart->content()->toArray());
        $order->total = $cart->total();
        $order->paid = false;
        $order->restaurants_id = $restaurant->id;

        $order->save();

        $order->raiseEvent( new OrderIsPlaced( $restaurant, $order, $cart ) );

        return $order;
    }

}