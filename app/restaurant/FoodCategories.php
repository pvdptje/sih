<?php


namespace App\Restaurant;

use Illuminate\Database\Eloquent\Model;

class FoodCategories extends Model{

    public $timestamps = false;


    public function Products()
    {
        return $this->hasMany('App\\Restaurant\\Product', 'food_categories_id');
    }


}