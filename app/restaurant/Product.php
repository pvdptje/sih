<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/20/14
 * Time: 11:05 PM
 */

namespace App\Restaurant;


use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    public $timestamps = false;



    public function extras()
    {
        return $this->belongsToMany('App\\Restaurant\\Extra', 'products_extras', 'products_id', 'extras_id');
    }

    public function decorator()
    {

        return (new Decorator)->setProduct($this);

    }


} 