<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 11/2/14
 * Time: 2:30 PM
 */

namespace App\Restaurant\Profile\Property;

class Email extends BaseProperty {

    protected $key = 'email';

    public $sortOrder = 2;

    protected function getPropertyValue()
    {
        $result = $this->db->table( $this->table )->where('key', $this->key)->where('restaurant_id', $this->restaurant->id)->take(1)->get();

        return !empty($result[0]->value) ? $result[0]->value : null;
    }

    protected function saveProperty()
    {
        $this->db->table( $this->table )->where('key', $this->key)->where('restaurant_id', $this->restaurant->id)->delete();

        $this->db->table( $this->table )->insert([
            'key' => $this->key,
            'value' => $this->property,
            'restaurant_id' => $this->restaurant->id
        ]);
    }

    /**
     * Returns a string of validation rules;
     * @return string
     */
    public function getValidationRules()
    {
        return 'required';
    }


    public function showForm($properties = array())
    {
        return $this->view->make('dashboard.restaurant.settings.profile.views.'.$this->key)->with([
                'form' => $this->formBuilder->text( $this->getPropertyName(), $this->property, $properties)
            ]
        );
    }



}