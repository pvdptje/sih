<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 11/2/14
 * Time: 2:30 PM
 */

namespace App\Restaurant\Profile\Property;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Logo extends BaseProperty {


    public $sortOrder = 3;


    /**
     * This must return the property value
     * @return mixed
     */
    protected function getPropertyValue()
    {
        $result = $this->db->table( $this->table )->where('key', 'logo')->where('restaurant_id', $this->restaurant->id)->take(1)->get();

        return !empty($result[0]->value) ? $result[0]->value : '';
    }

    /**
     *
     * @return null
     */
    protected function saveProperty()
    {



         if($this->property instanceof UploadedFile && $this->property->isValid())
         {
             $path = $this->restaurant->getUploadDirectoryPath();

             $this->property->move($path, 'logo.'.$this->property->getClientOriginalExtension());

             $uri = $this->restaurant->getUploadDirectoryPath(false).'logo.'. $this->property->getClientOriginalExtension();

             $this->db->statement("DELETE FROM profiles WHERE `key` = 'logo' AND restaurant_id = ?", array($this->restaurant->id));

             //'value' => $uri,

             $this->db->table('profiles')->insert([
                 'key' => 'logo',
                 'value' => $uri,
                 'restaurant_id' => $this->restaurant->id
             ]);
         }
    }

    /**
     * If theres already a logo uploaded
     * its no longer required
     * @return string
     */
    public function getValidationRules()
    {

        if(\Input::hasFile('logo'))
            return 'mimes:jpeg,png,gif';

        return '';
    }


    public function showForm($properties = array())
    {
        return $this->view->make('dashboard.restaurant.settings.profile.views.logo')->with([
                'form' => $this->formBuilder->file( $this->getPropertyName(), $properties),
                'logo' => $this->getPropertyValue()
            ]
        );
    }


}