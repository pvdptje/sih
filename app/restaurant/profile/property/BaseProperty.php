<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 11/1/14
 * Time: 11:31 PM
 */

namespace App\Restaurant\Profile\Property;


use App\Restaurant\Restaurant;
use Illuminate\Database\DatabaseManager;
use Illuminate\Html\FormBuilder;
use Illuminate\View\Factory;

abstract class BaseProperty {


    const DEFAULT_SORT_ORDER = 1000;



    public $sortOrder = self::DEFAULT_SORT_ORDER;

    public $property;

    public $originalProperty;

    /**
     * @var \Illuminate\Database\Connection
     */
    public $db;

    /**
     * @var \App\Restaurant\Restaurant
     */
    public $restaurant;


    public $table = 'profiles';


    public $formBuilder;


    public $view;

    /**
     * This must return the property value
     * @return mixed
     */
    abstract protected function getPropertyValue();

    /**
     *
     * @return null
     */
    abstract protected function saveProperty();


    /**
     * Must return a  string of validation rules;
     * @return string
     */
    abstract protected function getValidationRules();



    abstract protected function showForm($params);

    public function __construct( Restaurant $restaurant , DatabaseManager $manager, FormBuilder $formBuilder, Factory $view)
    {
        $this->restaurant =  $restaurant;
        $this->db = $manager->connection( $manager->getDefaultConnection() );
        $this->formBuilder = $formBuilder;
        $this->view = $view;
        $this->fillProperty( $this->getPropertyValue(), true);


    }

    protected function fillProperty( $property , $setOriginal = false )
    {
        $this->property = $property;

        if($setOriginal)
            $this->originalProperty  = $property;
    }


    public function save()
    {
        $this->saveProperty();
    }


    public function set($value)
    {
        $this->property = $value;
    }


    public function get()
    {
        return $this->property;
    }


    public function getPropertyName()
    {
        return snake_case( (new \ReflectionClass($this)) -> getShortName());
    }

    protected function propertyHasChanged()
    {
        return $this->property !== $this->originalProperty;
    }


    public function __destruct()
    {

       /*     if($this->propertyHasChanged())
            {
                $this->saveProperty();
            }*/

    }


    public function __toString()
    {
        return (string) $this->get();
    }

} 