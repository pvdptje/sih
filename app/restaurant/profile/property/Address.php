<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 11/1/14
 * Time: 10:54 PM
 */


namespace App\Restaurant\Profile\Property;



class Address extends BaseProperty{


    public $sortOrder = 1;


    /**
     * Return the string with validation rules
     * @return string
     */
    public function getValidationRules()
    {
       return 'required';
    }

    /**
     * Do anything nessecary to save the property
     */
    public function saveProperty()
    {

        $this->restaurant->address = $this;
        $this->restaurant->save();

    }

    /**
     * Returns the property value
     *
     * @return mixed
     */
    public function getPropertyValue()
    {

        return $this->restaurant->address;

    }


    public function showForm($properties = array())
    {
        return $this->view->make('dashboard.restaurant.settings.profile.views.address')->with([
                'form' => $this->formBuilder->text( $this->getPropertyName(), $this->getPropertyValue(), $properties)
            ]
        );
    }





}