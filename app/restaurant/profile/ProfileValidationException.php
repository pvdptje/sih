<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 11/2/14
 * Time: 12:24 AM
 */

namespace App\Restaurant\Profile;


class ProfileValidationException extends \Exception {


    protected $validator;


    public function __construct( $validator )
    {
        $this->validator = $validator;
    }

} 