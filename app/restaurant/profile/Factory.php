<?php

/**
 * This class gathers all the profile parts
 */

namespace App\Restaurant\Profile;

use App\Restaurant\Profile\Property\BaseProperty;
use App\Restaurant\Restaurant;

use Illuminate\Foundation\Application;


class Factory {


    protected $restaurant;

    protected $properties = [];

    protected $objects = [];

    protected $app;

    protected $config;

    protected $view;


    /**
     * @param mixed $validationRules
     */
    public function setValidationRules($validationRules)
    {
        $this->validationRules[] = $validationRules;
    }

    /**
     * @return mixed
     */
    public function getValidationRules()
    {
        return $this->validationRules;
    }

    function __construct( Restaurant $restaurant , Application $app   )
    {


        $this->restaurant = $restaurant;
        $this->app = $app;
        $this->config = $app['config']->get('profile');

        $this->loadProperties();

    }

    protected function loadProperties(  )
    {

       foreach($this->config['properties'] as $class => $description)
       {
           $shortName = snake_case($class);
           $class = $this->getFullClassName( $class );

           if(!class_exists( $class ))
           {
               throw new \Exception("Property class {$class} not found for profile");
           }

           $this->objects[$shortName] = $this->app->make( $class, [$this->restaurant]  );
       }

    }



    public function getObjects()
    {
        // Sort them

        $list = [];
        foreach($this->objects as $object)
        {
            if($object->sortOrder == BaseProperty::DEFAULT_SORT_ORDER)
            {
                $list[] = $object;
            }
            else
            {
                // Make sure that every object has a unique sorting order
                if(array_key_exists($object->sortOrder, $list)){
                    throw new \Exception(
                        sprintf("Duplicate sort order on profile class: %s - %s",
                            $object->getPropertyName(),
                            $list[$object->sortOrder]->getPropertyName())
                    );
                }

                $list[$object->sortOrder] = $object;
            }

        }

        ksort($list);

        return $list;
    }

    protected function getValidator()
    {
        $validationRules = [];
        $input = [];

        foreach($this->objects as $property)
        {
            $input[$property->getPropertyName()] = $property->get();
            $validationRules[$property->getPropertyName()] = $property->getValidationRules();

        }

        return $this->app['validator']->make($input, $validationRules);
    }

    protected function saveObjects()
    {
        foreach($this->objects as $property)
        {
            $property->save();
        }
    }

    public function save()
    {
        $validator = $this->getValidator();

        if(!$validator->fails())
        {
            $this->saveObjects();

            return true;
        }


        /**
         * @todo
         * catch it
         */

        pre($validator->messages());
        throw new ProfileValidationException( $validator->messages() );


    }

    protected function getFullClassName( $class )
    {
         return $this->config['namespace'] . $class;
    }


    public function __get( $property )
    {
        if(isset($this->objects[$property]))
           return $this->objects[$property];

        throw new \Exception("Property {$property} not found on profile");
    }



}