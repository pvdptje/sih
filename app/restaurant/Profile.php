<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 11/1/14
 * Time: 5:43 PM
 */


namespace App\Restaurant;


use Illuminate\Database\Eloquent\Model;

class Profile extends Model {

    public $table = 'profiles';


    public static function get(Restaurant $restaurant, $key)
    {
        return static::where('key', $key)->where('restaurant_id', $restaurant->id)->take(1)->get()->first();
    }

}