<?php



namespace App\Restaurant;


use App\Dashboard\Restaurant\FirstRestaurantIsRegistered;
use App\Eventing\EventGenerator;
use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model{


    use EventGenerator;

    static $unguarded = true;



    public function getUploadDirectoryPath( $private = true )
    {
        $this->createUploadDirectory();

        if($private)
            return getcwd().'/usercontent/uploads/'.$this->id.'/';

        return \Request::root().'/usercontent/uploads/'.$this->id.'/';
    }


    public function moderators()
    {
        return $this->belongsToMany('App\\Dashboard\\Moderator\\Moderator', 'moderators_restaurants', 'restaurants_id', 'moderators_id');
    }

    public function paymentProviders()
    {
        return $this->belongsToMany('App\\Payment\\AvailablePaymentMethod', 'available_payment_methods_restaurants', 'restaurants_id', 'available_payment_methods_id')->withPivot(['config', 'active']);
    }

    public function productExtras()
    {
        return $this->hasMany('App\\Restaurant\\Extra', 'restaurant_id', 'id');
    }

<<<<<<< HEAD

    /**
     * @param $subdomain
     * @return static
     */
    public static function getBySubdomain($subdomain)
    {
        return static::where('subdomain', $subdomain)->get()->first();
    }


=======
>>>>>>> 4b1aadab484400d82c6fa4af83ed2cb579493986

    public static function createFirst($data)
    {

        $restaurant = static::create((array) $data);

        $restaurant->raiseEvent( new FirstRestaurantIsRegistered( $restaurant ));

        return $restaurant;


    }


    public function deliveryZones()
    {
        return $this->hasMany('App\\Restaurant\\DeliveryZone', 'restaurant_id');
    }

    public function foodCategories()
    {
        return $this->hasMany('App\\Restaurant\\FoodCategories', 'restaurants_id');
    }

    /**
     * @return \App\Restaurant\Profile\Factory
     */
    public function getProfile()
    {
        return app()->make('App\\Restaurant\\Profile\\Factory', [$this]);
    }


    public function scopeActiveOnly( $q )
    {
        return $q->where('activated', 1);
    }

    protected function createUploadDirectory()
    {
        if (!is_dir(getcwd() . '/usercontent/uploads/' . $this->id . '/'))
            mkdir(getcwd() . '/usercontent/uploads/' . $this->id . '/', 0777, true);
    }


}