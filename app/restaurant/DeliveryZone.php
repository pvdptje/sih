<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/28/14
 * Time: 10:01 PM
 */

namespace App\Restaurant;


use Illuminate\Database\Eloquent\Model;

class DeliveryZone extends Model {

    public $table = 'delivery_zones';

    public $timestamps =  false;


    public function restaurant()
    {
        return $this->belongsTo('App\\Restaurant\\Restaurant', 'restaurant_id');
    }

}