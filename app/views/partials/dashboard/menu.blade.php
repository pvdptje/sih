<<<<<<< HEAD
<ul class="nav navbar-nav mainbar-nav">

    @if(!Auth::moderator()->check())

    <li class="{{ is_active('dashboard.login.index') }}"><a href="{{ URL::route('dashboard.login.index') }}">Inloggen</a></li>

    @else
<!--
    <li class="{{ is_active('dashboard.front.index') }}"><a href="{{ URL::route('dashboard.front.index') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
-->
<!--    <li class="{{ is_active('dashboard.restaurant.index') }}"> <a href="{{{ URL::route('dashboard.restaurant.index') }}}">Uw restaurants</a></li> -->

    @if(route_contains(['dashboard.restaurant', 'dashboard.profile', 'dashboard.payment'], ['dashboard.restaurant.index']))

    <li class="dropdown ">
        <a href="#restaurant" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-spoon"></i>
            Uw restaurant
            <span class="caret"></span>
        </a>

        <ul class="dropdown-menu" role="menu">
            <li class="{{ is_active('dashboard.restaurant.show') }}"><a href=" {{  URL::route('dashboard.restaurant.show', array($restaurant->id) ) }}">Restaurant</a></li>

            <li class="{{ is_active('dashboard.payment.setup.index') }}"><a href=" {{  URL::route('dashboard.payment.setup.index', array($restaurant->id) ) }}">Ideal gegevens</a></li>

            <li class="{{ is_active('dashboard.restaurant.settings.deliveryzones.index')}}"><a href="{{ URL::route('dashboard.restaurant.settings.deliveryzones.index', array($restaurant->id )) }}">Bezorggebieden instellen</a></li>

            <li class="{{ is_active('dashboard.profile.index') }}"><a href="{{ URL::route('dashboard.profile.index', array($restaurant->id )) }}">Restaurant instellingen</a></li>
=======
<nav class="nav-sidebar">
    <ul class="nav">
        @if(!Auth::moderator()->check())

        <li class="{{ is_active('dashboard.login.index') }}"><a href="{{ URL::route('dashboard.login.index') }}">Inloggen</a></li>

        @else

        <li class="{{ is_active('dashboard.front.index') }}"><a href="{{ URL::route('dashboard.front.index') }}">Dashboard home</a></li>

        <li class="{{ is_active('dashboard.restaurant.index') }}"> <a href="{{{ URL::route('dashboard.restaurant.index') }}}">Uw restaurants</a></li>

        @if(route_contains(['dashboard.restaurant', 'dashboard.payment'], ['dashboard.restaurant.index']))


        <li class="nav-divider"></li>

        <li class="{{ is_active('dashboard.restaurant.show') }}"><a href=" {{  URL::route('dashboard.restaurant.show', array($restaurant->id) ) }}">Restaurant</a></li>

        <li class="{{ is_active('dashboard.payment.setup.index') }}"><a href=" {{  URL::route('dashboard.payment.setup.index', array($restaurant->id) ) }}">Ideal gegevens</a></li>
>>>>>>> 4b1aadab484400d82c6fa4af83ed2cb579493986

        <li class=""><a href="#">Algemene instellingen</a></li>

<<<<<<< HEAD
        </ul>
    </li>


    <li class="{{ is_active('dashboard.restaurant.menu.index') }}"><a href=" {{  URL::route('dashboard.restaurant.menu.index', array($restaurant->id) ) }}"><i class="fa fa-tags"></i>Menukaart</a></li>

    <li class="{{ is_active('dashboard.restaurant.menu.productextras.index') }}" >

        <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" href="{{ URL::route('dashboard.restaurant.menu.productextras.index', array($restaurant->id)) }}"><i class="fa fa-plus"></i>Productextra's    <span class="caret"></span></a>


        <ul class="dropdown-menu" role="menu">
            <li class="{{ is_active('dashboard.restaurant.menu.productextras.index') }}"><a href=" {{  URL::route('dashboard.restaurant.menu.productextras.index', array($restaurant->id) ) }}">Productextra's</a></li>
            <li class="{{ is_active('dashboard.restaurant.menu.productextras.create') }}"><a href="{{ URL::route('dashboard.restaurant.menu.productextras.create', array($restaurant->id)) }}">Nieuwe productextra</a></li>
        </ul>

    </li>



    @endif



    <li class=""><a href="{{ URL::route('dashboard.login.logout') }}"><i class="fa fa-lock"></i>Uitloggen</a> </li>

    @endif

<!--
<li class="dropdown ">
    <a href="#contact" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
        <i class="fa fa-external-link"></i>
        Extra Pages
        <span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="page-notifications.html">
                <i class="fa fa-bell"></i>
                &nbsp;&nbsp;Notifications
            </a>
        </li>

        <li>
            <a href="ui-icons.html">
                <i class="fa fa-smile-o"></i>
                &nbsp;&nbsp;Font Icons
            </a>
        </li>

        <li class="dropdown-submenu">
            <a tabindex="-1" href="#">
                <i class="fa fa-ban"></i>
                &nbsp;&nbsp;Error Pages
            </a>

            <ul class="dropdown-menu">
                <li>
                    <a href="page-404.html">
                        <i class="fa fa-ban"></i>
                        &nbsp;&nbsp;404 Error
                    </a>
                </li>

                <li>
                    <a href="page-500.html">
                        <i class="fa fa-ban"></i>
                        &nbsp;&nbsp;500 Error
                    </a>
                </li>
            </ul>
        </li>


        <li class="dropdown-submenu">

            <a tabindex="-1" href="#">
                <i class="fa fa-lock"></i>
                &nbsp;&nbsp;Login Pages
            </a>

            <ul class="dropdown-menu">
                <li>
                    <a href="account-login.html">
                        <i class="fa fa-unlock"></i>
                        &nbsp;&nbsp;Login
                    </a>
                </li>

                <li>
                    <a href="account-login-social.html">
                        <i class="fa fa-unlock"></i>
                        &nbsp;&nbsp;Login Social
                    </a>
                </li>

                <li>
                    <a href="account-signup.html">
                        <i class="fa fa-star"></i>
                        &nbsp;&nbsp;Signup
                    </a>
                </li>

                <li>
                    <a href="account-forgot.html">
                        <i class="fa fa-envelope"></i>
                        &nbsp;&nbsp;Forgot Password
                    </a>
                </li>
            </ul>
        </li>

        <li class="divider"></li>

        <li>
            <a href="page-blank.html">
                <i class="fa fa-square-o"></i>
                &nbsp;&nbsp;Blank Page
            </a>
        </li>

    </ul>
</li>
-->

</ul>
=======
        <li class="nav-divider"></li>

        <li class="{{ is_active('dashboard.restaurant.menu.index') }}"><a href=" {{  URL::route('dashboard.restaurant.menu.index', array($restaurant->id) ) }}">Menukaart</a></li>

        <li class="{{ is_active('dashboard.restaurant.menu.productextras.index') }}"><a href="{{ URL::route('dashboard.restaurant.menu.productextras.index', array($restaurant->id)) }}">Productextra's</a></li>
        @endif

        <li class="nav-divider"></li>

        <li class=""> <a href="{{ URL::route('dashboard.login.logout') }}">Uitloggen</a> </li>

        @endif

    </ul>
</nav>
>>>>>>> 4b1aadab484400d82c6fa4af83ed2cb579493986
