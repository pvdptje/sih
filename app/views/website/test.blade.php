<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SnelinHuis.com</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
<style>
    .label.label-warning, .btn.btn-warning {
        color: #ffffff;
        background-color: #F0AD4E;
        background-image: linear-gradient(to bottom, #F0AD4E, #EB9316);
        border-color: #E38D13 #E38D13 #E38D13;
    }
    .label.label-warning, .btn.btn-warning:hover {
        color: #ffffff;
        background-color: #EB9316;
        background-image: linear-gradient(to bottom, #EB9316, #EB9316);
        border-color: #E38D13 #E38D13 #E38D13;
    }
body {
    font-family: 'Open Sans', Tahoma, sans-serif;
    font-size:13px;
}
.container {
        max-width: 997px !important;
}
.header {

    padding-top:15px;
    padding-bottom:15px;
}
.header .logo {
    float:left;

}
.orange {
    color:#f7941d;
}
.below-header .jumbotron {
    background-color: #FDFDFD;
    border-bottom:3px solid #8cc13b;
    padding-top:15px;
    padding-bottom:15px;
}
.below-header .jumbotron h2 {
    margin:0;
}
.entry {
    padding-top:7px;
    padding-bottom:8px;
    padding-left:8px;
    padding-right:7px;

    margin-bottom:15px;
}
.entry.featured {
    background-color: #e0f4ff;
}
.entry h3 {
    margin:0;
    font-size:15px;
    font-weight:bold;
}
.entry .logo {
    padding:8px;
    background:white;
    border:1px solid #d7ebf6;
    text-align:center;
}
@media screen and (min-width: 768px)
{

    .entry .logo img {
        max-width:100px;
        margin:0 auto;
        max-height:100px;
    }

    .entry .ribbon-wrapper-red {
        top: -6px;
        right: -13px;

    }
}

@media screen and (max-width: 768px)
{
    .header .logo {
        float: none;
        margin: 0 auto;
        display: table;

    }
    .entries {
        padding:15px;
    }
}
.entry .ribbon-wrapper {
    margin: 50px auto;
    width: 280px;
    height: 370px;
    background: white;
    border-radius: 10px;
    -webkit-box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
    -moz-box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
    position: relative;
    z-index: 90;
}
.entry .ribbon-wrapper-red {
    width: 124px;
    height: 124px;
    overflow: hidden;
    position: absolute;
    top: 0px;
    right: -6px;
}
.entry .ribbon-red {
    font: bold 10px Sans-Serif;
    color: #333;
    text-align: center;
    /*text-shadow: rgba(255,255,255,0.5) 0px 1px 0px;*/
    -webkit-transform: rotate(45deg);
    -moz-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    -o-transform: rotate(45deg);
    position: relative;
    padding: 7px 0;
    right: -33px;
    top: 10px;
    width: 115px;
    background-image: -webkit-linear-gradient(top, #cc0000 0%, #990000 100%);
    background-image: linear-gradient(to bottom, #cc0000 0%, #990000 100%);
    background-repeat: repeat-x;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffcc0000', endColorstr='#ff990000', GradientType=0);
    color: #ffffff;
    -webkit-box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.3);
    z-index: 1;
    text-transform: uppercase;
}
.entry .ribbon-red span {
    position: absolute;
    right: 16px;
    top: 7px;
}
.entry .ribbon-red:before,
.entry .ribbon-red:after {
    content: "";
    border-top: 3px solid #6e8900;
    border-left: 3px solid transparent;
    border-right: 3px solid transparent;
    position: absolute;
    bottom: -3px;
}
.entry .ribbon-red:before {
    left: 0;
}
.entry .ribbon-red:after {
    right: 0;
}
.entry .btn {
    position: relative;
    z-index: 100;
}

</style>

</head>
<body>


<div class="container">

    <div class="header">
         <div class="logo">
                <img src="/images/logo.jpg" alt="SnelinHuis.com" />
         </div>
        <div class="clearfix"></div>
    </div>

</div>
<div class="below-header">
    <div class="jumbotron">
        <div class="container">
            <h2>Restaurants in de buurt van <span class="orange">Maasland</span> </h2>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">Filter de resultaten</div>
                <div class="panel-body">
                    <p>Waar heb je trek in?</p>
                    <form class="form-inline mini" style="margin-bottom: 0px;">
                        <fieldset>
                            <?php $ar = array('Sushi', 'Chinees', 'Indisch', 'Snackbar', 'Shoarma');?>
                            @foreach(array('Sushi', 'Chinees', 'Indisch', 'Snackbar', 'Shoarma') as $ding)
                            <div class="row filter-row">
                                <div class="col-xs-6">
                                    <label for="{{$ding}}">{{ $ding }}</label>
                                </div>
                                <div class="col-xs-6">
                                    <input type="checkbox" id="{{ $ding }}" checked name="filter[]" />
                                </div>
                            </div>
                            @endforeach

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-sm-8">

            <div class="entries">

            @foreach(range(0,4) as $i)
            <div class="row entry featured">

                <div class="col-xs-3"><div class="logo row"><img class="img-responsive" src="/images/noimage.jpg" /></div>


                </div>
                <div class="col-xs-9">

                    <div class="ribbon-wrapper-red"><div class="ribbon-red">&nbsp;<span>Aanbevolen</span></div></div>
                    <h3>Sushi A20 Thuis</h3>
                    <?php $rand = array_rand($ar)?>
                    <span class="label label-warning"><?php echo $ar[$rand]?></span><br /><br />

                    <span class="meta">Broeksportlaan 21, <strong>Vlaardingen</strong></span> <br />
                    <span class="meta">Geopend tot 21:00</span>

                    <div class="row">
                        <a href="#" class="btn btn-warning btn-md pull-right"><span class="glyphicon glyphicon-play-circle"></span> Bestellen</a>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
            @endforeach

            @foreach(range(0,10) as $i)
            <div class="row entry">

                <div class="col-xs-3"><div class="logo row"><img src="/images/noimage.jpg" /></div>


                </div>
                <div class="col-xs-9">
                    <h3>Sushi A20 Thuis</h3>
                    <span class="meta">Broeksportlaan 21, <strong>Vlaardingen</strong></span>
                </div>
            </div>
            @endforeach

            </div>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<script>
    $('.mini-submenu').on('click',function(){
        $(this).next('.list-group').toggle('slide');
        $('.mini-submenu').hide();
    })
</script>
</body>
</html>