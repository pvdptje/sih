<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SnelinHuis.com</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
    <link href='/css/custom.css' rel='stylesheet' type='text/css'>

</head>
<body>


<div class="container">

    <div class="header">
        <div class="logo">
            <img src="/images/logo.jpg" alt="SnelinHuis.com" />
        </div>
        <div class="clearfix"></div>
    </div>

</div>

<?php


$string = $restaurants->count() > 1 ? 'restaurants' : ($restaurants->count() == 0 ? 'restaurants' : 'restaurant');

?>
<div class="below-header">
    <div class="jumbotron">
        <div class="container">
            <h2 class="page-title">Bezorgrestaurants in de buurt van <span class="orange">{{{ $city }}}</span></h2>
            <p> {{{ $restaurants->count() }}} {{{ $string }}} gevonden. </p>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">Filter de resultaten</div>
                <div class="panel-body">
                    <p>Waar heb je trek in?</p>
                    <form class="form-inline mini" style="margin-bottom: 0px;">
                        <fieldset>
                            <?php $ar = array('Sushi', 'Chinees', 'Indisch', 'Snackbar', 'Shoarma');?>
                            @foreach(array('Sushi', 'Chinees', 'Indisch', 'Snackbar', 'Shoarma') as $ding)
                            <div class="row filter-row">
                                <div class="col-xs-6">
                                    <label for="{{$ding}}">{{ $ding }}</label>
                                </div>
                                <div class="col-xs-6">
                                    <input type="checkbox" id="{{ $ding }}" checked name="filter[]" />
                                </div>
                            </div>
                            @endforeach

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-sm-8">

            <div class="entries">


                @foreach($restaurants as $restaurant)

                <div class="row entry featured">

                    <div class="col-xs-3"><div class="logo row"> <img src="{{{ $restaurant->getProfile()->logo }}}" /></div>



                    </div>
                    <div class="col-xs-9">

                        <h3>{{{ $restaurant->name }}}</h3>
                        <?php $rand = array_rand($ar)?>
                        <span class="label label-warning"><?php echo $ar[$rand]?></span><br /><br />
                        <span class="meta">{{{ $restaurant->address }}}, <strong>{{{ $restaurant->city }}}</strong></span> <Br />

                        <span class="meta">Geopend tot 21:00</span>

                        <!--<div class="ribbon-wrapper-red"><div class="ribbon-red">&nbsp;<span>Aanbevolen</span></div></div>-->


                        <div class="row">
                            <a href="{{ subdomain_link($restaurant) }}" class="btn btn-warning btn-md pull-right"><span class="glyphicon glyphicon-play-circle"></span> Bestellen</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<script>
    $('.mini-submenu').on('click',function(){
        $(this).next('.list-group').toggle('slide');
        $('.mini-submenu').hide();
    })
</script>
</body>
</html>
