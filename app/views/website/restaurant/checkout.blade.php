<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{{ $restaurant->name }}} | Snel in huis.com</title>
    <meta name="description" content="Je favoriete van eten {{{ $restaurant->name }}} snel in huis!">
    <meta name="author" content="pvdp">

    <!-- Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700,300&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="plugins/flexslider/flexslider.css" rel="stylesheet">
    <link href="css/animations.css" rel="stylesheet">
    <link href="plugins/owl-carousel/owl.carousel.css" rel="stylesheet">

    <!-- iVega core CSS file -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
    <link href="css/skins/orange.css" rel="stylesheet">

    <!-- Custom css -->
    <link href="css/skins/website.css" rel="stylesheet">
</head>

<body class="front no-slideshow" style="min-width:768px;">

<!-- page wrapper start (Add "boxed" class to page-wrapper in order to enable boxed layout mode) -->
<div class="page-wrapper">

<!-- scrollToTop start -->
<div class="scrollToTop"><i class="fa fa-angle-up"></i></div>
<!-- scrollToTop end -->

<!-- preheader start -->
<div class="preheader">
    <div class="container">
        <div class="preheader-content">
            <div class="row">
                <div class="col-md-12">
                    <h2>Drop us a line</h2>
                    <div class="row">
                        <form role="form" id="preheader-contact-form">
                            <div class="col-md-6">
                                <div class="form-group name">
                                    <label for="name3">Name*</label>
                                    <input type="text" class="form-control" id="name3" placeholder="" name="name3">
                                </div>
                                <div class="form-group email">
                                    <label for="email3">Email*</label>
                                    <input type="email" class="form-control" id="email3" placeholder="" name="email3">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group message">
                                    <label for="message3">Message*</label>
                                    <textarea class="form-control" rows="5" id="message3" placeholder="" name="message3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" value="Submit" class="btn btn-default">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="trigger">
        <a href="javascript:showPreHeader()" class="triangle"><i class="fa fa-plus"></i></a>
    </div>
</div>
<!-- preheader end -->

<!-- header top start -->
<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <ul class="info">
                    <li><i class="fa fa-spoon"></i> <span class="primary"> {{{ $restaurant->name }}}.</span>snel in huis.com </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="social-links text-right">
                    <li><a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                    <li><a target="_blank" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
                    <li><a target="_blank" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a></li>
                    <li><a target="_blank" href="http://www.skype.com"><i class="fa fa-skype"></i></a></li>
                    <li><a target="_blank" href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
                    <li><a target="_blank" href="http://www.pinterest.com"><i class="fa fa-pinterest"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- header top end -->

<!-- header start -->
<header class="header text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="logo">
                    <a href="{{{ Request::root() }}}"><img id="logo" src="{{{ $profile->logo }}}" style="max-width:200px" alt="Je favoriete eten van {{{ $restaurant->name }}} snel in huis!"></a>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header end -->

<!-- main-navigation start (remove fixed class from main-navigation in order to disable fixed navigation mode)-->
<div class="main-navigation fixed">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <!-- navbar start -->
                <nav class="navbar navbar-default" role="navigation">

                    <!-- Toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                        <ul class="nav navbar-nav ">
                            <li class="active">
                                <a href="{{{ Request::root() }}}" class="dropdown-toggle" data-toggle="dropdown">Bestellen</a>
                            </li>
                            <li>
                                <a href="#">Reserveren</a>
                            </li>
                            <li>
                                <a href="#">Openingstijden</a>
                            </li>
                            <li>
                                <a href="#">Meer informatie</a>
                            </li>
                            <li>
                                <a href="#">Contact</a>
                            </li>
                        </ul>


                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- main-navigation end -->


<div class="section-main">
    <div class="container">

        { Form::open(array('route' => 'store.front.store')) }
        <div class="col-md-4 col-xs-12 three-left-col">

            <h4>Adresgegevens</h4>

            <div class="form-group">
                   <span class="validation-error">
                {{{ $errors->first('firstname') }}}
                    </span>
                <label for="">Voornaam</label>
                {{ Form::text('firstname', null, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                   <span class="validation-error">
                {{{ $errors->first('lastname') }}}
                       </span>
                <label for="">Achternaam</label>
                {{ Form::text('lastname', null, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-xs-8">
                        <div>
                               <span class="validation-error">
                            {{{ $errors->first('postcode') }}}

                                   </span>

                            <label for="">Postcode </label>
                        </div>
                        <div class="form-group">
                            {{ Form::text('postcode', null, array('class' => 'form-control')) }}
                        </div>
                    </div>
                    <div class="col-xs-4">
                         <span class="validation-error">
                            {{{ $errors->first('housenumber') }}}
                                   </span>
                        <label for="">Huisnummer</label>
                        <div class="form-group">
                            {{ Form::text('housenumber', null, array('class' => 'form-control')) }}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="form-group">
                <span class="validation-error">

                {{{ $errors->first('street') }}}
                </span>
                <label for="">Straat</label>
                {{ Form::text('street', null, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                   <span class="validation-error">
                {{{ $errors->first('city') }}}
                       </span>
                <label for="">Woonplaats</label>
                {{ Form::text('city', null, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                   <span class="validation-error">
                {{{ $errors->first('email') }}}
                       </span>
                <label for="">Email</label>
                {{ Form::text('email', null, array('class' => 'form-control')) }}
            </div>

            <!--     <div class="form-group">

                     <label><input type="checkbox"> Afleveren op een ander adres?</label>

                 </div>-->

        </div>

        <div class="col-md-4 col-xs-12 middle-col">

            <div class="form-group">
                   <span class="validation-error">
                {{{ $errors->first('paymentmethod') }}}
                       </span>
                <label for="">Betaalmethode</label>
                {{ Form::select('type', array('Ideal', 'Contant'), null, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                   <span class="validation-error">
                {{{ $errors->first('bank') }}}
                   </span>
                <label for="">Kies uw bank</label>
                 {{ Form::select('type', $banks, null, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">


                <label>{{ Form::checkbox('newsletter', 1) }} Ik wil graag de nieuwsbrief van {restaurant name} ontvangen</label>

            </div>

        </div>

        <div class="col-md-4 col-xs-12">

            <h4>Uw bestelling</h4>

            <table class="table">
                <tr>

                    <td>Receptenkalender 2014 (zoutloos)</td>
                    <td>1 stuk</td>
                    <td>&euro; 9,95</td>

                </tr>
                <tr>

                    <td>Transactiekosten</td>
                    <td></td>
                    <td>&euro; 0,45</td>

                </tr>
                <tr>

                    <td>Bezorgkosten</td>
                    <td></td>
                    <td>&euro; 2,95</td>

                </tr>
                <tr>

                    <td><strong>Totaal</strong></td>
                    <td></td>
                    <td><strong>&euro; 12,35</strong></td>

                </tr>
            </table>

            <hr />


            {{ Form::submit('Bestellen', array('class' => 'btn btn-default')) }}

        </div>
        {{ Form::close() }}


    </div>
</div>

<div id="test">

</div>
<footer>

    <!-- .footer start -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="footer-content">
                        <div class="logo-footer"><img src="{{{ $profile->logo }}}" alt="Bestel {{{$restaurant->name }}}" class="img-responsive"  /></div>

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="footer-content">

                        <p>In dit hippe bruine eetcafé kan je heerlijk ongedwongen genieten. Niet alleen van lekker eten en drinken, maar ook van goed gezelschap. Ga voor een goede kop koffie met wat zoets, een snelle lunch, een borrel of een smaakvol, betaalbaar diner. Je bent altijd van harte welkom!</p>

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="footer-content">
                        <ul class="info">
                            <li><i class="fa fa-map-marker"></i> {{{ $profile->address }}}</li>
                            <li><i class="fa fa-phone"></i> {{{ $profile->phone }}} </li>
                            <li><i class="fa fa-envelope-o"></i> {{{ $profile->email }}} </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .footer end -->

    <!-- .subfooter start -->
    <div class="subfooter">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <p>Copyright &copy; SnelinHuis</p>
                </div>
                <div class="col-md-8">
                    <nav class="navbar navbar-default" role="navigation">
                        <!-- Toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-collapse-2">
                            <ul class="nav navbar-nav">
                                <li><a href="/">Home</a></li>
                                <li><a href="/restaurant-aanmelden">Restaurant aanmelden</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- .subfooter end -->

</footer>
<!-- footer end -->

</div>
<!-- page wrapper end -->

<!-- JavaScript files placed at the end of the document so the pages load faster
================================================== -->
<!-- Jquery and Bootstap core js files -->
<script type="text/javascript" src="plugins/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

<!-- Modernizr javascript -->
<script type="text/javascript" src="plugins/modernizr.js"></script>

<!-- Flexslider javascript -->
<script type="text/javascript" src="plugins/flexslider/jquery.flexslider.js"></script>

<!-- Owl carousel javascript -->
<script type="text/javascript" src="plugins/owl-carousel/owl.carousel.js"></script>

<!-- Magnific Popup javascript -->
<script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Appear javascript -->
<script type="text/javascript" src="plugins/jquery.appear.js"></script>

<!-- Count To javascript -->
<script type="text/javascript" src="plugins/jquery.countTo.js"></script>

<!-- Flowtype javascript -->
<script type="text/javascript" src="plugins/flowtype.js"></script>

<!-- Parallax javascript -->
<script src="plugins/jquery.parallax-1.1.3.js"></script>

<!-- Contact form -->
<script src="plugins/jquery.validate.js"></script>

<!-- Initialization of Plugins -->
<script type="text/javascript" src="js/template.js"></script>

<link rel="stylesheet" href="/js/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />

<script src="/js/fancybox/jquery.fancybox.js"></script>


</body>
</html>
