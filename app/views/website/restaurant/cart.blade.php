<ul class="nav navbar-nav navbar-right navbar-symbols ">
    <li class="dropdown keep-open cartli">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

            <div class="cart">
                <i class="fa fa-shopping-cart"></i> <?php echo $cart->count()?> producten - <?php echo format_price( $cart->total() ) ?> <i class="fa fa-caret-down"></i>
            </div>

        </a>
        <ul class="dropdown-menu animated fadeInDownSmall">
            <li>

                <table class="table cart-table">

                    <?php foreach($cart->content() as $row){ ?>


                        <tr>
                            <td><input type="text" size="2" class="" onchange="changeAmount($(this), '{{{ $row->rowid }}}')" value="{{{ $row->qty }}}" name="qty" /> </td>
                            <td><span class="name">{{{ $row->name }}}</span>


                <span class="options">

                 @foreach($row->options as $option)


                    {{{$option}}}


                @endforeach
                </span>





                            </td>
                            <td>{{{ format_price ( $row->subtotal ) }}} </td>

                            <td><div class="delete"><i class="fa fa-trash-o" onclick="removeProduct('{{{ $row->rowid }}}')"></i></div></td>
                        </tr>
                    <?php } ?>
                    <tr>

                        <td colspan="4" class="total">Totaal {{{ format_price ( $cart->total() ) }}} </td>
                    </tr>
                    @if($cart->count() !== 0)
                    <tr>
                        <td colspan="4">

                            <a href="{{ URL::route('website.checkout.index', array($subdomain))}}"><button class="btn btn-sm">Afrekenen</button></a>

                        </td>
                    </tr>
                    @endif

                </table>

            </li>
        </ul>
    </li>
</ul>



<script>
    $(function() {

        var dropdown = $('.dropdown.keep-open');
        console.log(dropdown);
        $(document).on('click.bs.dropdown.data-api', function (e) {
            // See if clicked element is outside the dropdown.
            if (dropdown.find(e.target).length === 0) {
                dropdown.removeClass('open');
            }
        });
        dropdown.on("hide.bs.dropdown", function(e) {
            return false;
        });


        function changeAmount()
        {
            alert(3)
        }
    });
</script>