<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{{ $restaurant->name }}} | Snel in huis.com</title>
    <meta name="description" content="Online eten bestellen bij {{{ $restaurant->name }}} ">
    <meta name="author" content="Patrick van der Pols">

    <!-- Mobile Meta -->
  <!--  <meta name="viewport" content="width=device-width, initial-scale=1.0">-->

    <!-- Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700,300&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="plugins/flexslider/flexslider.css" rel="stylesheet">
    <link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/animations.css" rel="stylesheet">
    <link href="plugins/owl-carousel/owl.carousel.css" rel="stylesheet">

    <!-- iVega core CSS file -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Custom css -->
    <link href="css/custom.css" rel="stylesheet">
</head>

<body class="not-front">

<!-- page wrapper start (Add "boxed" class to page-wrapper in order to enable boxed layout mode) -->
<div class="page-wrapper">

<!-- scrollToTop start -->
<div class="scrollToTop"><i class="fa fa-angle-up"></i></div>
<!-- scrollToTop end -->

<!-- preheader start -->
<div class="preheader">
    <div class="container">
        <div class="preheader-content">
            <div class="row">
                <div class="col-md-12">
                    <h2>Drop us a line</h2>
                    <div class="row">
                        <form role="form" id="preheader-contact-form">
                            <div class="col-md-6">
                                <div class="form-group name">
                                    <label for="name3">Name*</label>
                                    <input type="text" class="form-control" id="name3" placeholder="" name="name3">
                                </div>
                                <div class="form-group email">
                                    <label for="email3">Email*</label>
                                    <input type="email" class="form-control" id="email3" placeholder="" name="email3">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group message">
                                    <label for="message3">Message*</label>
                                    <textarea class="form-control" rows="5" id="message3" placeholder="" name="message3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" value="Submit" class="btn btn-default">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="trigger">
        <a href="javascript:showPreHeader()" class="triangle"><i class="fa fa-plus"></i></a>
    </div>
</div>
<!-- preheader end -->

<!-- header top start -->
<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <ul class="info">
                    <li><i class="fa fa-map-marker"></i> {{{ $profile->address }}}</li>
                    <li><i class="fa fa-phone"></i> {{{ $profile->phone }}} </li>
                    <li><i class="fa fa-envelope-o"></i> {{{ $profile->email }}} </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="social-links text-right">
                    <li><a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                    <li><a target="_blank" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
                    <li><a target="_blank" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a></li>
                    <li><a target="_blank" href="http://www.skype.com"><i class="fa fa-skype"></i></a></li>
                    <li><a target="_blank" href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
                    <li><a target="_blank" href="http://www.pinterest.com"><i class="fa fa-pinterest"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- header top end -->

<!-- header start -->
<header class="header text-center">
    <div class="container">

            <div class="col-md-12">
                <div class="logo">
                    <a href="{{{ Request::root() }}}"><img id="logo" src="{{{ $profile->logo }}}" style="max-width:200px" alt="Je favoriete eten van {{{ $restaurant->name }}} snel in huis!"></a>
                </div>
                <div class="site-slogan">
                    {{{ $profile->slogan }}}
                </div>
            </div>

    </div>
</header>
<!-- header end -->

<!-- main-navigation start (remove fixed class from main-navigation in order to disable fixed navigation mode)-->
<div class="main-navigation fixed">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <!-- navbar start -->
                <nav class="navbar navbar-default" role="navigation">

                    <!-- Toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-main-menu">
                            <li class="active">
                                <a href="index.html" class="dropdown-toggle" data-toggle="dropdown">Home</a>

                            </li>

                        </ul>
                        <div id="cart">

                        </div>
                    </div>
                </nav>
                <!-- navbar end -->

            </div>
        </div>
    </div>
</div>
<!-- main-navigation end -->

<!-- main start -->
<section class="main">

    <!-- page intro start -->
    <div class="page-intro">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="page-title">Online bestellen bij {{{ $restaurant->name }}}</h1>
                </div>
                <div class="col-md-6">
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="active"><i class="fa fa-home">{{{ $restaurant->name }}}</i>Home</li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page intro end -->

    <!-- main content wrapper start -->
    <div class="main-content-wrapper">
        <div class="container">
            <div class="row">

                <!-- main content start -->
                <section class="main-content col-md-8">

                    <table class="table">

                        @foreach($restaurant->foodCategories as $category)
                        <thead>
                            <tr id="{{{$category->name}}}">
                                <th colspan="3"> {{{ $category->name }}}  </th>
                            </tr>
                        </thead>


                        @foreach($category->products as $product)

                        <tr>

                            <td> {{{ $product->name }}}</td>
                            <td> {{{ format_price($product->price) }}}</td>
                            <td class="text-right"> <button onclick="cartAdd({{{$product->id}}})"><i class="fa fa-plus"></i></button></td>
                        </tr>


                        @endforeach

                        @endforeach
                    </table>

                </section>
                <!-- main content end -->

                <!-- sidebar start -->
                <aside class="sidebar col-md-3 col-md-offset-1">
                    <div class="block clearfix">
                        <h3>Kies een categorie</h3>
                        <nav>
                            <ul class="nav nav-pills nav-stacked">
                                @foreach($restaurant->foodCategories as $category)

                                <li><a href="#{{{$category->name}}}">{{{ $category->name }}}</a></li>

                                @endforeach
                            </ul>
                        </nav>
                    </div>
                    <div class="block clearfix">
                        <h3>Latest tweets</h3>
                        <ul class="tweets">
                            <li>
                                <i class="fa fa-twitter"></i>
                                <p><a href="#">@lorem</a> ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, aliquid, et molestias nesciunt <a href="#">http://t.co/dzLEYGeEH9</a>.</p><span>16 hours ago</span>
                            </li>
                            <li>
                                <i class="fa fa-twitter"></i>
                                <p><a href="#">@lorem</a> ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, aliquid, et molestias nesciunt <a href="#">http://t.co/dzLEYGeEH9</a>.</p><span>16 hours ago</span>
                            </li>
                        </ul>
                    </div>
                    <div class="block clearfix">
                        <h3>Text Sample</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos, nemo, necessitatibus, expedita voluptate esse ipsam aliquid blanditiis maxime sequi veniam suscipit atque sapiente cum voluptatum quos mollitia laborum? Esse, officia!</p>
                    </div>
                    <div class="block clearfix">
                        <h3>Testimonial</h3>
                        <ul class="testimonials">
                            <li>
                                <i class="fa blockquote"></i>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, aliquid, et molestias nesciunt.</p><span class="name">- John doe</span><a href="#" class="company"> Company</a>
                            </li>
                            <li>
                                <i class="fa blockquote"></i>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, aliquid, et molestias nesciunt.</p><span class="name">- John doe</span><a href="#" class="company"> Company</a>
                            </li>
                        </ul>
                    </div>
                    <div class="block clearfix">
                        <h3>Portfolio</h3>
                        <div class="gallery row">
                            <div class="gallery-item col-xs-4">
                                <a href="portfolio-item.html" class="mask-wrapper">
                                    <img src="images/gallery-1.jpg" alt="">
                                    <span class="mask"><span class="small triangle"><i class="fa fa-link"></i></span></span>
                                </a>
                            </div>
                            <div class="gallery-item col-xs-4">
                                <a href="portfolio-item.html" class="mask-wrapper">
                                    <img src="images/gallery-2.jpg" alt="">
                                    <span class="mask"><span class="small triangle"><i class="fa fa-link"></i></span></span>
                                </a>
                            </div>
                            <div class="gallery-item col-xs-4">
                                <a href="portfolio-item.html" class="mask-wrapper">
                                    <img src="images/gallery-3.jpg" alt="">
                                    <span class="mask"><span class="small triangle"><i class="fa fa-link"></i></span></span>
                                </a>
                            </div>
                            <div class="gallery-item col-xs-4">
                                <a href="portfolio-item.html" class="mask-wrapper">
                                    <img src="images/gallery-4.jpg" alt="">
                                    <span class="mask"><span class="small triangle"><i class="fa fa-link"></i></span></span>
                                </a>
                            </div>
                            <div class="gallery-item col-xs-4">
                                <a href="portfolio-item.html" class="mask-wrapper">
                                    <img src="images/gallery-5.jpg" alt="">
                                    <span class="mask"><span class="small triangle"><i class="fa fa-link"></i></span></span>
                                </a>
                            </div>
                            <div class="gallery-item col-xs-4">
                                <a href="portfolio-item.html" class="mask-wrapper">
                                    <img src="images/gallery-6.jpg" alt="">
                                    <span class="mask"><span class="small triangle"><i class="fa fa-link"></i></span></span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="block">
                        <h3>Tags</h3>
                        <div class="tags-cloud">
                            <div class="tag">
                                <a href="#">energy</a>
                            </div>
                            <div class="tag">
                                <a href="#">business</a>
                            </div>
                            <div class="tag">
                                <a href="#">food</a>
                            </div>
                            <div class="tag">
                                <a href="#">fashion</a>
                            </div>
                            <div class="tag">
                                <a href="#">finance</a>
                            </div>
                            <div class="tag">
                                <a href="#">culture</a>
                            </div>
                            <div class="tag">
                                <a href="#">health</a>
                            </div>
                            <div class="tag">
                                <a href="#">sports</a>
                            </div>
                            <div class="tag">
                                <a href="#">life style</a>
                            </div>
                            <div class="tag">
                                <a href="#">books</a>
                            </div>
                        </div>
                    </div>
                    <div class="block bb-clear clearfix">
                        <form role="search">
                            <div class="form-group search">
                                <input type="text" class="form-control" placeholder="Search">
                            </div>
                        </form>
                    </div>
                </aside>
                <!-- sidebar end -->

            </div>
        </div>
    </div>
    <!-- main content wrapper end -->

</section>
<!-- main end -->

<!-- footer start -->
<footer>

    <!-- .footer start -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="footer-content">
                        <div class="logo-footer"><img id="logo-footer" src="images/logo_blue_footer.png" alt=""></div>
                        <p>Lorem ipsum dolor sit amet, consect tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim ven. Lorem ipsum dolor sit amet elit.</p>
                        <ul class="info">
                            <li><i class="fa fa-map-marker"></i> One infinity loop, 54100</li>
                            <li><i class="fa fa-phone"></i> +00 1234567890 &amp; +00 1234567891 </li>
                            <li><i class="fa fa-envelope-o"></i> info@ivega.com</li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="footer-content">
                        <h2>Latest tweets</h2>
                        <ul class="tweets">
                            <li>
                                <i class="fa fa-twitter"></i>
                                <p><a href="#">@lorem</a> ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, aliquid, et molestias nesciunt <a href="#">http://t.co/dzLEYGeEH9</a>.</p><span>16 hours ago</span>
                            </li>
                            <li>
                                <i class="fa fa-twitter"></i>
                                <p><a href="#">@lorem</a> ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, aliquid, et molestias nesciunt <a href="#">http://t.co/dzLEYGeEH9</a>.</p><span>16 hours ago</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="footer-content">
                        <h2>Latest Projects</h2>
                        <div class="gallery row">
                            <div class="gallery-item col-xs-4">
                                <a href="portfolio-item.html" class="mask-wrapper">
                                    <img src="images/gallery-1.jpg" alt="">
                                    <span class="mask"><span class="small triangle"><i class="fa fa-link"></i></span></span>
                                </a>
                            </div>
                            <div class="gallery-item col-xs-4">
                                <a href="portfolio-item.html" class="mask-wrapper">
                                    <img src="images/gallery-2.jpg" alt="">
                                    <span class="mask"><span class="small triangle"><i class="fa fa-link"></i></span></span>
                                </a>
                            </div>
                            <div class="gallery-item col-xs-4">
                                <a href="portfolio-item.html" class="mask-wrapper">
                                    <img src="images/gallery-3.jpg" alt="">
                                    <span class="mask"><span class="small triangle"><i class="fa fa-link"></i></span></span>
                                </a>
                            </div>
                            <div class="gallery-item col-xs-4">
                                <a href="portfolio-item.html" class="mask-wrapper">
                                    <img src="images/gallery-4.jpg" alt="">
                                    <span class="mask"><span class="small triangle"><i class="fa fa-link"></i></span></span>
                                </a>
                            </div>
                            <div class="gallery-item col-xs-4">
                                <a href="portfolio-item.html" class="mask-wrapper">
                                    <img src="images/gallery-5.jpg" alt="">
                                    <span class="mask"><span class="small triangle"><i class="fa fa-link"></i></span></span>
                                </a>
                            </div>
                            <div class="gallery-item col-xs-4">
                                <a href="portfolio-item.html" class="mask-wrapper">
                                    <img src="images/gallery-6.jpg" alt="">
                                    <span class="mask"><span class="small triangle"><i class="fa fa-link"></i></span></span>
                                </a>
                            </div>
                            <div class="gallery-item col-xs-4">
                                <a href="portfolio-item.html" class="mask-wrapper">
                                    <img src="images/gallery-7.jpg" alt="">
                                    <span class="mask"><span class="small triangle"><i class="fa fa-link"></i></span></span>
                                </a>
                            </div>
                            <div class="gallery-item col-xs-4">
                                <a href="portfolio-item.html" class="mask-wrapper">
                                    <img src="images/gallery-8.jpg" alt="">
                                    <span class="mask"><span class="small triangle"><i class="fa fa-link"></i></span></span>
                                </a>
                            </div>
                            <div class="gallery-item col-xs-4">
                                <a href="portfolio-item.html" class="mask-wrapper">
                                    <img src="images/gallery-9.jpg" alt="">
                                    <span class="mask"><span class="small triangle"><i class="fa fa-link"></i></span></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .footer end -->

    <!-- .subfooter start -->
    <div class="subfooter">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <p>Copyright © 2014 iVega. All Rights Reserved</p>
                </div>
                <div class="col-md-8">
                    <ul class="info">
                        <li><i class="fa fa-map-marker"></i> {{{ $profile->address }}}</li>
                        <li><i class="fa fa-phone"></i> {{{ $profile->phone }}} </li>
                        <li><i class="fa fa-envelope-o"></i> {{{ $profile->email }}} </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- .subfooter end -->

</footer>
<!-- footer end -->

</div>
<!-- page wrapper end -->

<!-- JavaScript files placed at the end of the document so the pages load faster
================================================== -->
<!-- Jquery and Bootstap core js files -->
<script type="text/javascript" src="plugins/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

<!-- Modernizr javascript -->
<script type="text/javascript" src="plugins/modernizr.js"></script>

<!-- Flexslider javascript -->
<script type="text/javascript" src="plugins/flexslider/jquery.flexslider.js"></script>

<!-- Owl carousel javascript -->
<script type="text/javascript" src="plugins/owl-carousel/owl.carousel.js"></script>

<!-- Magnific Popup javascript -->
<script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Appear javascript -->
<script type="text/javascript" src="plugins/jquery.appear.js"></script>

<!-- Count To javascript -->
<script type="text/javascript" src="plugins/jquery.countTo.js"></script>

<!-- Flowtype javascript -->
<script type="text/javascript" src="plugins/flowtype.js"></script>

<!-- Contact form -->
<script src="plugins/jquery.validate.js"></script>

<!-- Initialization of Plugins -->
<script type="text/javascript" src="js/template.js"></script>

<link rel="stylesheet" href="/js/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />

<script src="/js/fancybox/jquery.fancybox.js"></script>
<script>


    function getCart()
    {
        $.get('{{URL::route('website.cart.index', array($subdomain))}}').done ( function ( response ) {
        $("#cart").html(response);
    });
    }
    function removeProduct( key )
    {
        $.post('{{{ Request::root() }}}/cart/delete/'+key).done ( function ( response )
        {

            getCart();

        });

    }
    function cartAdd( id )
    {
        $.post('{{{ Request::root() }}}/cart/add/'+id).done ( function ( response )
        {

            if(response.extra == true)
            {
                $.fancybox( {
                    type: 'iframe',
                    href: response.href,
                    afterShow: function(){
                        $('.fancybox-inner').css('min-height', ($(window).height() - 300))
                    }
                } );

            } else {
                getCart();
            }

        });

    }

    $(function() {

        // Smooth scrolling to anchors
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });

        getCart();

        // offset for cart and menu to float
        var offsetPixels = 200;

        $(window).scroll(function() {
            if ($(window).scrollTop() > offsetPixels) {
                $( ".scrollingBox" ).css({
                    "position": "fixed",
                    "top": "65px"
                });
            } else {
                $( ".scrollingBox" ).css({
                    "position": "relative",
                    "top": "0"
                });
            }
            if ($(window).scrollTop() > offsetPixels) {
                $( ".cartScrollingBox" ).css({
                    "position": "fixed",
                    "top": "65px"
                });
            } else {
                $( ".cartScrollingBox" ).css({
                    "position": "relative",
                    "top": "0"
                });
            }
        });

    });

</script>
</body>
</html>
