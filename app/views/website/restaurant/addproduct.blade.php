@extends('dashboard.restaurant.popup')

@section('content')
<strong>{{{ $product->name }}}</strong>

<hr />


    @if(Session::has('message'))
        <div class="alert alert-danger">
                {{{ Session::get('message') }}}
        </div>
    @endif



<?php
$showFields = function( $extra ){

    $output = '';

    switch($extra->type)
    {
        case 'single_choice':

            $output .= '<thead> <tr><th>';
            $output .= $extra->name;
            if($extra->required)
            {
                $output .= ' <span class="required">Verplichte keuze</span>';
            }
            $output .= '</th></tr> </thead>';



            foreach($extra->values as $value)
            {
                $output .= '<tr><td><label>';
                $output .= Form::radio($extra->id, $value->id) .  $value->name .' ';
                $output .= '<span class="label label-warning">';
                $output .= $value->price != 0 ?'+ '. format_price($value->price) : null ;
                $output .= '</span>';
                $output .= '</label></td></tr>';

            }

        break;
    }

    return $output;
}; ?>

<form method="post" action="{{{URL::route('website.cart.config', array($subdomain, $product->id))}}}">

    <table class="table">

@foreach($product->extras as $extra)


    <?php echo $showFields($extra) ?>

@endforeach

    </table>
    <div class="clearfix"></div>
    <input type="submit" style="color:white" class="btn btn-default btn-xs" value="Toevoegen" />
</form>



@stop

@section('javascript')

@if(Session::has('done'))
<script>
    $( function() {
        parent.getCart();
        parent.$.fancybox.close();
    });
</script>
@endif

@stop
