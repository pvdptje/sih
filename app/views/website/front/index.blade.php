<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SnelinHuis.com</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
    <link href='/css/custom.css' rel='stylesheet' type='text/css'>

</head>
<body>

<div id="wrap">
<div class="container">

    <div class="header">
        <div class="logo">
            <img src="/images/logo.jpg" alt="SnelinHuis.com" />
        </div>
        <div class="clearfix"></div>
    </div>

</div>

<div class="below-header">
    <div class="jumbotron">
        <div class="container">
            <h2 class="page-title">Snel en gemakkelijk online eten bestellen.</h2>

        </div>
    </div>
</div>

    <div class="home-cta">
        <div class="row">

            <div class="container">
                <div class="col-xs-12 form">


                <span class="text-center">
                    <h2>Voer je postcode in</h2>
                </span>

                    {{ Form::open(array('route' => 'website.front.store')) }}
                    <div class="row">
                        <div class="col-sm-10 col-xs-12">
                            {{ Form::text('zipcode', null, array('class' => 'form-control zipfield larger', 'placeholder' => '1234AB')) }}
                        </div>
                        <div class="col-sm-2 col-xs-12">
                            <button type="submit" class="btn btn-warning" id="order-button"><i class="fa fa-arrow-circle-right"></i> Honger!</button>
                        </div>



                    </div>
                    {{ Form::close() }}


                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div id="push"></div>
</div>

<div id="footer">
    <div class="container text-center">
        <p class="pull-left">Snelinhuis &copy {{ date('Y') }}</p>
        <div class="pull-right">
            <ul class="nav nav-pills">
               <li role="presentation">  <a href="#">Over snelinhuis.com</a></li>
               <li><a href="#">Restaurant aanmelden</a></li>
               <li><a href="#">Contact</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<script>
    $('.mini-submenu').on('click',function(){
        $(this).next('.list-group').toggle('slide');
        $('.mini-submenu').hide();
    })
</script>
</body>
</html>

