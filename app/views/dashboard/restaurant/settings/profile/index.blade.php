@extends('dashboard.master')



@section('content')


{{ Form::open(array('route' => array('dashboard.profile.store', $restaurant->id), 'files' => true)) }}
<p>Op deze pagina kunt u het profiel van uw restaurant aanpassen</p>


<?php $objects = $profile->getObjects() ?>
@foreach($objects as $object)

{{ $object->showForm( array('class' => 'form-control')) }}


@endforeach

{{ Form::submit() }}

{{ Form::close() }}

@stop