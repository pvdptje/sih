@extends('dashboard.master')



@section('content')
<link rel="stylesheet" href="/js/chosen/chosen.css">
<style>
    #loading {
       display:none;
       padding-top:50px;
       padding-bottom:50px;
       border:1px solid #eee;
       width:450px;
       margin:0 auto;

        .loader img {
            max-width:100px;
        }
    }

</style>

@if(Session::has('message'))

<div class="alert alert-warning"> {{{ Session::get('message') }}}</div>

@endif

@if($errors->has())

<div class="alert alert-warning">


</div>


@endif

<div id="message"></div>
<h2 class="title">Bezorggebieden instellen</h2>


<p>Selecteer hieronder in welke plaatsen uw restaurant bezorgd. U kunt altijd bepaalde wijken uitsluiten.</p>


<div id="loading">
<div class="loader text-center">
    <img src="/images/loader.gif" alt="Aan het laden">
    <h3 id="text"></h3>
</div>


</div>
<form id="zones">
<select id="cities" data-placeholder="Kies een aantal plaatsen"  class="chosen form-control" multiple name="cities[]">

</select>


    <button id="save"><i class="fa fa-save"></i> Opslaan</button>
</form>



@stop

@section('javascript')


<script src="/js/chosen/chosen.jquery.js"></script>
<script>
     $( function ( ) {

     $("[rel=tooltip]").tooltip({ placement: 'left'});

     var citiesCombo = $("#cities");

     $.ajax({
            url: '{{URL::route("dashboard.restaurant.settings.deliveryzones.getzones", array($restaurant->id))}}?postcode=<?php echo $postcode->postcode?>',
            cache: false,
            method: 'get',
            beforeSend: function(){
                $("#text").html('Woonplaatsen aan het verzamelen..');
                $('#loading').show();
            },
            complete: function(){

                $('#loading').hide();
                citiesCombo.show();

            },
            success: function( response ){

                $.each ( response.zones, function (  )
                {
                    citiesCombo.append('<option value="'+this.city+'" selected>'+this.city+'</option>');

                });
                $.each ( response.postcodes, function (  )
                {
                    citiesCombo.append('<option value="'+this.city+'">'+this.city+'</option>');

                });

                setTimeout ( function() {
                        citiesCombo.chosen();
                }, 200);
            }
        });




         $("#save").click ( function (e) {
             e.preventDefault();

             $.ajax({
                 url: '{{URL::route("dashboard.restaurant.settings.deliveryzones.store", array($restaurant->id))}}',
                 cache: false,
                 data: $("#zones").serialize(),
                 method: 'post',
                 beforeSend: function(){
                     $("#text").html('Opslaan...');
                     $('#loading').show();
                 },
                 complete: function(){

                     $('#loading').hide();

                 },
                 success: function( response ){

                     $("#message").html('<div class="alert alert-success">Bezorggebieden opgeslagen</div>');

                 }
             });
         });

     });
</script>

@stop