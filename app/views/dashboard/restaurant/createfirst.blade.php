@extends('dashboard.master')


@section('content')

<link rel="stylesheet" href="/js/chosen/chosen.css" />

            <!-- sidebar start -->
            @include('partials.dashboard.menu')
            <!-- sidebar end -->

            <!-- main content start -->
            <section class="main-content col-md-8 col-md-offset-1">

                @if(Session::has('message'))

                <div class="alert alert-warning"> {{{ Session::get('message') }}}</div>

                @endif

                @if($errors->has())

                <div class="alert alert-warning">


                    <div class="alert alert-danger">

                        <p>Er zijn validatiefouten opgetreden, verbeter aub de volgende fouten:</p>

                        <br />
                        <ul>
                            @foreach($errors->all() as $error)

                            <li> {{ $error }} </li>

                            @endforeach
                        </ul>

                    </div>


                </div>


                @endif

                <h2 class="title">Restaurant registeren</h2>

                <div class="alert alert-warning">
                    <p>Via deze pagina kunt u uw restaurant registeren. Voordat u deze registreert, zorg dat u tenminste 1 geldige paymentprovider heeft. Staat uw payment provider niet in de keuzelijst? <a href="">Klik hier</a></p>
                </div>

                {{ Form::open(array('route' => 'dashboard.restaurant.storeFirst')) }}

                <div class="form-group ">

                    <label class="">
                        Gewenste subdomein
                    </label>
                    <p class="small">Link naar uw bestelformulier word bijvoorbeeld <span class="primary">uwrestaurant</span>.snelinhuis.com</p>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon">http://</span>
                                {{ Form::text('subdomain', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="row" style="margin-top:7px;">
                                <h3 class="no-margin">.snelinhuis.com</h3></div>
                            </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>
                        Naam van restaurant
                    </label>


                        {{ Form::text('name', $application->name, array('class' => 'form-control')) }}

                </div>
                <div class="form-group  <?php echo $errors->has('street') || $errors->has('housenumber') ? 'has-error' : null?>">

                        <div class="row">
                            <div class="col-sm-8">
                                <label>Straatnaam</label>

                                {{ Form::text('street', $application->street, array('class' => 'form-control')) }}
                            </div>

                            <div class="col-sm-4">
                                <label>Huisnummer</label>

                                {{ Form::text('housenumber', $application->housenumber , array('class' => 'form-control')) }}
                            </div>
                        </div>


                </div>
                <div class="form-group <?php echo $errors->has('postcode') || $errors->has('city') ? 'has-error' : null?>">



                        <div class="form-group row <?php echo $errors->has('postcode') || $errors->has('city') ? 'has-error' : null?>">
                            <div class="col-sm-4">
                                <label>Postcode</label>
                                {{ Form::text('postcode', $application->postcode, array('class' => 'form-control')) }}
                            </div>

                            <div class="col-sm-8">
                                <label>Plaatsnaam</label>
                                {{ Form::text('city', $application->city, array('class' => 'form-control')) }}
                            </div>
                        </div>

                </div>
                <div class="form-group">
                    <label>
                        Telefoon
                    </label>


                    {{ Form::text('phonenumber', null, array('class' => 'form-control')) }}

                </div>

                <div class="form-group  <?php echo $errors->has('cocnumber') ? 'has-error' : null?>">


                        <label>KVK Nummer</label>

                        {{ Form::text('cocnumber', $application->cocnumber, array('class' => 'form-control')) }}



                </div>

                <hr />

                <div class="form-group  <?php echo $errors->has('termsofservice') ? 'has-error' : null?>">

                    <label>Payment providers</label>

                    <p class="small">Vink aan bij welke payment providers u bent aangesloten</p>


                    {{ Form::select('paymentProviders[]', $payment_providers, null, array('id' => 'paymentproviders','multiple', 'data-placeholder' => '', 'tabindex' => '11','class' => 'chosen-select-no-results form-control')) }}


                </div>

                <input type="hidden" name="toc" value="false" />
                {{ Form::checkbox('toc', 1) }}


                {{ Form::submit() }}

                {{ Form::close() }}

                <hr />



@stop

@section('javascript')
<script src="/js/chosen/chosen.jquery.js"></script>
<script>
    $( function ( ) {
       // $("[rel=tooltip]").tooltip({ placement: 'left'});


        $("#paymentprovider").attr('')

        var config = {
            '.chosen-select'           : {},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Niets gevonden'},
            '.chosen-select-width'     : {width:"95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }


        $('table tr td').each ( function( ){

            $(this).click ( function( ){

                checkbox = $(this).find('[type=checkbox]');

                if(checkbox.prop('checked'))
                    checkbox.prop('checked', false);
                else
                    checkbox.prop('checked', true);
            })
        })

    });
</script>

@stop