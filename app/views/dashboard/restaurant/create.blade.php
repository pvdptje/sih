@extends('dashboard.master')


@section('content')

<div class="page-intro">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="page-title">Dashboard </h2>
            </div>
            <div class="col-md-6">
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li><a href="{{{ URL::route('dashboard.front.index') }}}"><i class="fa fa-home"></i>Dashboard</a></li>
                        <li><a href="{{{ URL::route('dashboard.restaurant.index') }}}">Restaurants</a></li>
                        <li class="active"><a href="{{{ URL::route('dashboard.restaurant.create') }}}">Restaurant registreren</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- page intro end -->

<!-- main content wrapper start -->
<div class="main-content-wrapper">
    <div class="container">
        <div class="row">

            <!-- sidebar start -->
            @include('partials.dashboard.menu')
            <!-- sidebar end -->

            <!-- main content start -->
            <section class="main-content col-md-8 col-md-offset-1">

                @if(Session::has('message'))

                <div class="alert alert-warning"> {{{ Session::get('message') }}}</div>

                @endif

                @if($errors->has())

                <div class="alert alert-warning">


                </div>


                @endif

                <h2 class="title">Restaurant registeren</h2>

                <div class="alert alert-warning">
                    <p>Via deze pagina kunt u uw restaurant registeren. Voordat u deze registreert, zorg dat u tenminste 1 geldige paymentprovider heeft. Staat uw payment provider niet in de keuzelijst? <a href="">Klik hier</a></p>
                </div>




                <hr />


            </section>
            <!-- main content end -->

        </div>
    </div>
</div>
<!-- main content wrapper end -->

@stop

@section('javascript')

<script>
    $( function ( ) {
        $("[rel=tooltip]").tooltip({ placement: 'left'});
    });
</script>

@stop