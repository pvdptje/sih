@extends('dashboard.master')

<<<<<<< HEAD
@section('title')
Productextra's
@stop
=======
>>>>>>> 4b1aadab484400d82c6fa4af83ed2cb579493986

@section('content')


@if(Session::has('message'))

<div class="alert alert-warning"> {{{ Session::get('message') }}}</div>

@endif

@if($errors->has())

<div class="alert alert-warning">


</div>


@endif


<<<<<<< HEAD
<p>Product extra's zijn opties die uw klant kan kiezen tijdens het bestellen van een product. </p>

<a class="btn btn-default" href="{{ URL::route('dashboard.restaurant.menu.productextras.create', array($restaurant->id)) }}">
    <i class="fa fa-plus"></i> Nieuwe product extra aanmaken
</a>
=======
<h2>Product extra's</h2>
<hr />

<p>Product extra's zijn opties die uw klant kan kiezen tijdens het bestellen van een product. </p>

<a class="btn btn-default" href="{{ URL::route('dashboard.restaurant.menu.productextras.create', array($restaurant->id)) }}"> <i class="fa fa-plus"></i> Nieuwe product extra aanmaken</a>
>>>>>>> 4b1aadab484400d82c6fa4af83ed2cb579493986

<table class="table table-striped custab">
    <thead>
    <tr>
        <th>Naam</th>
        <th>Verplicht</th>
        <th style="width:10%">Actie</th>
    </tr>

    </thead>

    <tbody>
        @if(!$restaurant->productExtras->isEmpty())
            @foreach($restaurant->productExtras as $extra)

        <tr>
            <td>{{{ $extra->name }}}</td>
            <td><?php echo $extra->required ? 'Ja' : 'Nee' ?></td>
            <td>
                <div class="btn-group">

                    <button title="Bewerken" rel="tooltip"><a href="{{ URL::route('dashboard.restaurant.menu.productextras.edit', array($restaurant->id, $extra->id)) }}"><i class="fa fa-pencil"></i> </a></button>
                    <button title="Verwijderen" onclick="confirmDelete('{{ URL::route('dashboard.restaurant.menu.productextras.delete', array($restaurant->id, $extra->id)) }}')"rel="tooltip"><i class="fa fa-trash-o"></i></button>                </div>
            </td>
        </tr>

            @endforeach
        @else

        <tr>
            <td colspan="3">U heeft nog geen product extra's</td>
        </tr>

        @endif
    </tbody>
</table>




<a class="btn btn-primary" href="{{URL::previous()}}">&laquo; Vorige pagina</a>

@stop

@section('javascript')

<script>
    var tooltips = function(){
        $("[rel=tooltip]").tooltip({ placement: 'left'});
    };


    $(function() {
        tooltips();
    })


</script>

@stop