@extends('dashboard.master')

<<<<<<< HEAD
@section('title')
Nieuwe productextra
@stop
=======
>>>>>>> 4b1aadab484400d82c6fa4af83ed2cb579493986

@section('content')


@if(Session::has('message'))

<div class="alert alert-warning"> {{{ Session::get('message') }}}</div>

@endif

@if($errors->has())

<div class="alert alert-warning">


</div>


@endif

<<<<<<< HEAD
<div class="portlet">

    <div class="portlet-header">
        <h3>
            <i class="fa fa-bar-chart-o"></i>
            Nieuwe productextra
        </h3>
    </div> <!-- /.portlet-header -->

    <div class="portlet-content">

        <div class="row row-marginless">


            {{ Form::open(array('route' => array('dashboard.restaurant.menu.productextras.store', $restaurant->id))) }}

            <div class="form-group">

                <table class="table custab">
                    <tr>
                        <td colspan="1">Naam van productextra</td>
                        <td colspan="3" class="text-right"><label rel="tooltip" title="Geeft aan of deze extra een verplichte keuze is"><input name="required" type="checkbox" value="1" /> Verplicht</label></td>
                    </tr>
                    <tr>
                        <td colspan="4">{{ Form::text('name', null, array('class' => 'form-control')) }} </td>
                    </tr>
                    <tr>
                        <td colspan="4">Type</td>
                    </tr>
                    <tr>
                        <td colspan="4">

                            <label rel="tooltip" title="De klant krijgt mag 1 extra kiezen">  <input type="radio" name="type" value="single_choice" /> Enkele keuze  </label> <br />
                            <label rel="tooltip" title="De klant krijgt mag meerdere extra's kiezen"><input type="radio" name="type" value="multiple_choice" /> Meerkeuze </label> <br/>

                            <br />

                            <label rel="tooltip" title="Indien meerkeuze, geef aan hoeveel keuzes de klant mag maken">Aantal keuzes</label>
                            <br /><small>0 is onbeperkt</small>
                            <input type="text"  rel="tooltip" title="Indien meerkeuze, geef aan hoeveel keuzes de klant mag maken" class="form-control" name="max_choices"  value="" placeholder="0" />
                        </td>
                    </tr>
                    <tr id="beforeoptionrow">
                        <td colspan="4">Waarden</td>
                    </tr>

                    <tr class="optionrow">
                        <td><input type="text" class="form-control" name="extravalue[name][]" placeholder="Naam (bijv knoflooksaus)" /></td>
                        <td><input type="text" class="form-control" name="extravalue[price][]" placeholder="0.50" /></td>
                        <td colspan="2" style="width:10%"> <button rel="tooltip" title="Waarde verwijderen" class="deleteOptionRow"><i class="fa fa-minus" ></i> </button></td>
                    </tr>


                    <tr>
                        <td colspan="4">
                            <button rel="tooltip" id="addOption" title="Nog een waarde toevoegen" class=""><i class="fa fa-plus" ></i> </button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="text-right">
                            <button rel="tooltip"  title="Opslaan" id="save" class="btn btn-primary">Opslaan</button>
                        </td>
                    </tr>
                </table>

            </div>

            {{ Form::close() }}


        </div> <!-- /.row -->




    </div> <!-- /.portlet-content -->

</div>


=======

<h2>Nieuwe productextra</h2>
<hr />

{{ Form::open(array('route' => array('dashboard.restaurant.menu.productextras.store', $restaurant->id))) }}

<div class="form-group">

    <table class="table custab">
        <tr>
            <td colspan="1">Naam van productextra</td>
            <td colspan="3" class="text-right"><label rel="tooltip" title="Geeft aan of deze extra een verplichte keuze is"><input name="required" type="checkbox" value="1" /> Verplicht</label></td>
        </tr>
        <tr>
            <td colspan="4">{{ Form::text('name', null, array('class' => 'form-control')) }} </td>
        </tr>
        <tr>
            <td colspan="4">Type</td>
        </tr>
        <tr>
            <td colspan="4">

                <label rel="tooltip" title="De klant krijgt mag 1 extra kiezen">  <input type="radio" name="type" value="single_choice" /> Enkele keuze  </label> <br />
                <label rel="tooltip" title="De klant krijgt mag meerdere extra's kiezen"><input type="radio" name="type" value="multiple_choice" /> Meerkeuze </label> <br/>

                <br />

                <label rel="tooltip" title="Indien meerkeuze, geef aan hoeveel keuzes de klant mag maken">Aantal keuzes</label>
                <br /><small>0 is onbeperkt</small>
                <input type="text"  rel="tooltip" title="Indien meerkeuze, geef aan hoeveel keuzes de klant mag maken" class="form-control" name="max_choices"  value="" placeholder="0" />
            </td>
        </tr>
        <tr id="beforeoptionrow">
            <td colspan="4">Waarden</td>
        </tr>

        <tr class="optionrow">
            <td><input type="text" class="form-control" name="extravalue[name][]" placeholder="Naam (bijv knoflooksaus)" /></td>
            <td><input type="text" class="form-control" name="extravalue[price][]" placeholder="0.50" /></td>
            <td colspan="2" style="width:10%"> <button rel="tooltip" title="Waarde verwijderen" class="deleteOptionRow"><i class="fa fa-minus" ></i> </button></td>
        </tr>


        <tr>
            <td colspan="4">
                <button rel="tooltip" id="addOption" title="Nog een waarde toevoegen" class=""><i class="fa fa-plus" ></i> </button>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="text-right">
                <button rel="tooltip"  title="Opslaan" id="save" class="btn btn-primary">Opslaan</button>
            </td>
        </tr>
    </table>

</div>

{{ Form::close() }}
>>>>>>> 4b1aadab484400d82c6fa4af83ed2cb579493986


<a class="btn btn-primary" href="{{URL::previous()}}">&laquo; Vorige pagina</a>

@stop

@section('javascript')

<script>
    var tooltips = function(){
        $("[rel=tooltip]").tooltip({ placement: 'left'});
    };


    var optionsRowsDeletable = function ()
    {


        $('.deleteOptionRow').click ( function () {


            $(this).closest('.optionrow').remove();



        });

    };

    var common = function()
    {
        tooltips();
        optionsRowsDeletable();
    };


    $( function ( ) {


        common();

        $("#addOption").click ( function( e ){
            e.preventDefault();
            selector = '.optionrow:last';

            if($('.optionrow').length < 1)
            {
                selector = '#beforeoptionrow';
            }

            $(selector).after(
                '<tr class="optionrow">' +
                    '<td><input type="text" class="form-control" name="extravalue[name][]" placeholder="Naam (bijv knoflooksaus)" /></td>' +
                    '<td><input type="text" class="form-control" name="extravalue[price][]" placeholder="0.50" /></td>' +
                    '<td style="width:10%"> <button rel="tooltip" title="Waarde verwijderen" class="deleteOptionRow" class=""><i class="fa fa-minus" ></i> </button></td>' +
                    '</tr>');

            common();
        });


    });

</script>

@stop