@extends('dashboard.master')


@section('content')

<style>
    fieldset {
        border:1px solid #ccc;
        padding:15px;
    }
</style>
                @if(Session::has('message'))

                <div class="alert alert-warning"> {{{ Session::get('message') }}}</div>

                @endif

                @if($errors->has())

                <div class="alert alert-warning">


                </div>


                @endif


                <h2>Menukaart bewerken</h2>
                <hr />


                <div id="menucart">

                </div>

                <div class="form-group">

                    <fieldset>

                        <label class="control-label">Nieuwe productgroep</label>
                        <input type="text" class="form-control" id="food_category" name="food_category" />

                        <br />
                        <button id="addfoodgroup"><i class="fa fa-plus"></i> Toevoegen </button>
                    </fieldset>




@stop

@section('javascript')

<script>

    function getMenu()
    {
        $.get( "{{URL::route('dashboard.restaurant.menu.getmenu', array($restaurant->id))}}", function( response ) {

            $( "#menucart" ).html( response );

        });

    }
    $( function ( ) {

        getMenu();
        $("[rel=tooltip]").tooltip({ placement: 'left'});

        $("#addfoodgroup").click ( function() {

            group = $("#food_category").val();
            $.post('{{URL::route('dashboard.restaurant.menu.addgroup', array($restaurant->id))}}', {group:group}).done ( function ( response ) {

               getMenu();


            });

        });

    });

</script>

@stop