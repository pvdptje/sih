@extends('dashboard.restaurant.popup')

@section('content')
<form id="form">
<table class="table table-striped custab">

    <thead>
        <tr>
            <th colspan="3">Product bewerken</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">Naam</td>
            <td>{{ Form::text('name', $product->name, array('class' => 'form-control')) }}</td>
        </tr>
        <tr>
            <td colspan="2">Prijs <br /><small>inclusief btw</small></td>
            <td>{{ Form::text('price', $product->price, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td colspan="3">
                <strong>Product extra's</strong>
            </td>
        </tr>
        @if($restaurant->productExtras->isEmpty())

        <tr>
            <td colspan="3">
                Er zijn nog geen productextra's aangemaakt. <a onclick="window.top.location.href = 'url'">Maak deze eerst aan.</a>
            </td>
        </tr>

        @else

        <tr>
            <td colspan="3">

        @if(!$product->extras->isEmpty())


        <?php
        $list = [];
        foreach($product->extras as $pExtra){

            $list[] = $pExtra->id;

        } ?>

  <select data-placeholder="Kies extra's" class="chosen form-control" name="extras[]"  multiple>

            @foreach($restaurant->productExtras as $extra)

            <option value="{{$extra->id}}" <?php echo in_array($extra->id, $list) ? 'selected' : null ?>>{{{ $extra->name }}}</option>

            @endforeach

       </select>

            </td>
        </tr>

        @else

        <tr>
            <td colspan="3">

        <select data-placeholder="Kies extra's" name="extras[]" class="chosen form-control" multiple>

            @foreach($restaurant->productExtras as $extra)



            <option value="{{$extra->id}}">{{{ $extra->name }}}</option>

            @endforeach

        </select>

          </td>
        </tr>

        @endif


        @endif

        <tr>
            <td colspan="3">


                <button id="save"><i class="fa fa-save"></i> Opslaan</button>


            </td>
        </tr>



    </tbody>
</table>
</form>

@stop

@section('javascript')
<script>
    $(function()
    {
        $('.chosen').chosen();

        $("#save").click ( function ( e) {
            e.preventDefault();

            var formData = $("#form").serialize();
            $.post('{{URL::route('dashboard.restaurant.menu.product.update', array($restaurant->id, $product->id)) }}', formData).done ( function ( response ) {

                parent.getMenu();
                parent.$.fancybox.close();


            });
        });

    });
</script>
<script src="/js/chosen/chosen.jquery.js"></script>


@stop

