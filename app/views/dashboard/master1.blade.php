<!DOCTYPE html>
<html lang="en">
<head>


    <meta charset="utf-8">
    <title>Snel in huis.com</title>
    <meta name="description" content="Je favoriete eten snel in huis!">
    <meta name="author" content="pvdp">

    <base href="/">
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700,300&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="plugins/flexslider/flexslider.css" rel="stylesheet">
    <link href="css/animations.css" rel="stylesheet">
    <link href="plugins/owl-carousel/owl.carousel.css" rel="stylesheet">

    <!-- iVega core CSS file -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
    <link href="css/skins/orange.css" rel="stylesheet">

    <!-- Custom css -->
    <link href="css/skins/custom.css" rel="stylesheet">


</head>

<body class="front no-slideshow">

<!-- page wrapper start (Add "boxed" class to page-wrapper in order to enable boxed layout mode) -->
<div class="page-wrapper">

<!-- scrollToTop start -->
<div class="scrollToTop"><i class="fa fa-angle-up"></i></div>
<!-- scrollToTop end -->

<!-- preheader start -->
<div class="preheader">
    <div class="container">
        <div class="preheader-content">
            <div class="row">
                <div class="col-md-12">
                    <h2>Support</h2>
                    <div class="row">
                        <form role="form" id="preheader-contact-form">
                            <div class="col-md-6">
                                <div class="form-group name">
                                    <label for="name3">Name*</label>
                                    <input type="text" class="form-control" id="name3" placeholder="" name="name3">
                                </div>
                                <div class="form-group email">
                                    <label for="email3">Email*</label>
                                    <input type="email" class="form-control" id="email3" placeholder="" name="email3">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group message">
                                    <label for="message3">Message*</label>
                                    <textarea class="form-control" rows="5" id="message3" placeholder="" name="message3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" value="Submit" class="btn btn-default">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="trigger">
        <a href="javascript:showPreHeader()" class="triangle"><i class="fa fa-plus"></i></a>
    </div>
</div>
<!-- preheader end -->

<!-- header top start -->
<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <ul class="info">
                    <li><img id="logo" style="max-width:150px" src="images/logo_blue.png" alt="Je favoriete eten snel in huis!"></li>
                </ul>
            </div>

        </div>
    </div>
</div>
<!-- header top end -->

<!-- header start -->

<!-- header end -->

    <section class="main">


            @yield('content')


    </section>


<footer>

    <!-- .footer start -->
    <div class="footer" style="padding:0;">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="footer-content">
                        <div class="logo-footer"></div>
                        <p>Lorem ipsum dolor sit amet, consect tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim ven. Lorem ipsum dolor sit amet elit.</p>
                        <ul class="info">
                            <li><i class="fa fa-map-marker"></i> Infinite loop 52</li>
                            <li><i class="fa fa-phone"></i> +31 1234567890 </li>
                            <li><i class="fa fa-envelope-o"></i> info@snelinhuis.com</li>
                        </ul>

                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- .footer end -->

    <!-- .subfooter start -->
    <div class="subfooter">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <p>Copyright &copy; SnelinHuis</p>
                </div>
                <div class="col-md-8">
                    <nav class="navbar navbar-default" role="navigation">
                        <!-- Toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-collapse-2">
                            <ul class="nav navbar-nav">
                                <li><a href="/">Home</a></li>
                                <li><a href="/restaurant-aanmelden">Restaurant aanmelden</a></li>
                                <li><a href="blog.html">Dashboard</a></li>

                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- .subfooter end -->

</footer>
<!-- footer end -->

</div>
<!-- page wrapper end -->

<!-- JavaScript files placed at the end of the document so the pages load faster
================================================== -->
<!-- Jquery and Bootstap core js files -->
<script type="text/javascript" src="plugins/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

<!-- Modernizr javascript -->
<script type="text/javascript" src="plugins/modernizr.js"></script>

<!-- Flexslider javascript -->
<script type="text/javascript" src="plugins/flexslider/jquery.flexslider.js"></script>

<!-- Owl carousel javascript -->
<script type="text/javascript" src="plugins/owl-carousel/owl.carousel.js"></script>

<!-- Magnific Popup javascript -->
<script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Appear javascript -->
<script type="text/javascript" src="plugins/jquery.appear.js"></script>

<!-- Count To javascript -->
<script type="text/javascript" src="plugins/jquery.countTo.js"></script>

<!-- Flowtype javascript -->
<script type="text/javascript" src="plugins/flowtype.js"></script>

<!-- Parallax javascript -->
<script src="plugins/jquery.parallax-1.1.3.js"></script>

<!-- Contact form -->
<script src="plugins/jquery.validate.js"></script>

<!-- Initialization of Plugins -->
<script type="text/javascript" src="js/template.js"></script>

@yield('javascript')


</body>
</html>
