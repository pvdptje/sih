@extends('dashboard.master')


@section('content')


                @if(Session::has('message'))

                <div class="alert alert-warning"> {{{ Session::get('message') }}}</div>

                @endif

                @if($errors->has())

                <div class="alert alert-warning">


                </div>


                @endif

                <h2 class="title">Laatste bestellingen</h2>



                <table class="table table-striped custab">

                    <thead>
                        <tr>
                            <th>Referentie</th>
                            <th>Restaurant</th>
                            <th>Datum</th>
                            <th>Bedrag</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>

                        @foreach(range(1,10) as $number)
                        <tr>
                            <td>{{ $number }}</td>
                            <td>Klaasvaak</td>
                            <td>15 - 01 - 1989</td>
                            <td>Bedrag</td>
                            <td class="text-right"><a href="#"><i class="fa fa-search"  rel="tooltip" title="Bestelling bekijken""></i></a></td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>



@stop

@section('javascript')

<script>
    $( function ( ) {
        $("[rel=tooltip]").tooltip({ placement: 'left'});
    });
</script>

@stop