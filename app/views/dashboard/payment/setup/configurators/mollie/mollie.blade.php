
    <td colspan="3">

        <p>Uw apikey</p>
        {{ Form::text('mollie_apikey', $params, array('class' => 'form-control', 'id' => 'mollie')) }}

        <br />

        <button class="save btn-primary btn-small btn pull-right">Opslaan</button>


        <div class="clearfix"></div>

        <script>
            $( function () {
                $('.save').click( function() {
                    value = $('#mollie').val();
                    $.post('{{ URL::route('dashboard.payment.setup.store', array($restaurant->id, $providerID)) }}', {apikey: value}).done ( function( response ) {
                        var success = $("#success");
                        var error = $("#error");
                        if(response == '{{ \App\Dashboard\Payment\Setup\ResponseCodes::SUCCESS }}' ){
                            $('.{{$providerID}}').fadeOut();
                            error.fadeOut();
                            success.fadeIn();
                            success.html('<p>Mollie apikey succesvol getest en opgeslagen.</p>');
                        } else {
                            success.fadeOut();
                            error.html(response);
                            error.fadeIn();
                        }

                    });
                });
            })
        </script>

    </td>


