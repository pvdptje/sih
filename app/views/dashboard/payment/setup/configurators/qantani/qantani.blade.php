
<td colspan="3">

    <p>Merchant Id</p>
    {{ Form::text('merchant_id', $params['merchantID'], array('class' => 'form-control', 'id' => 'qantani_merchant_id')) }}

    <p>Merchant Key</p>
    {{ Form::text('merchant_key', $params['merchantKey'], array('class' => 'form-control', 'id' => 'qantani_merchant_key')) }}

    <p>Merchant Secret</p>
    {{ Form::text('merchant_secret', $params['merchantSecret'], array('class' => 'form-control', 'id' => 'qantani_merchant_secret')) }}
    <br />

    <button class="save{{$providerID}} btn-primary btn-small btn pull-right">Opslaan</button>


    <div class="clearfix"></div>

    <script>
        $( function () {
            $('.save{{$providerID}}').click( function() {

                merchantID = $('#qantani_merchant_id').val();
                merchantKey = $('#qantani_merchant_key').val();
                merchantSecret = $('#qantani_merchant_secret').val();

                $.post('{{ URL::route('dashboard.payment.setup.store', array($restaurant->id, $providerID)) }}', {merchantID: merchantID, merchantKey:merchantKey, merchantSecret: merchantSecret}).done ( function( response ) {
                    var success = $("#success");
                    var error = $("#error");
                    if(response == '{{ \App\Dashboard\Payment\Setup\ResponseCodes::SUCCESS }}' ){
                        $('.{{$providerID}}').fadeOut();
                        error.fadeOut();
                        success.fadeIn();

                        success.html('<p>Qantani gegevens succesvol opgeslagen.</p>');


                    } else {
                        success.html('');
                        success.fadeOut();
                        error.html(response);
                        error.fadeIn();
                    }

                });
            });
        })
    </script>

</td>


