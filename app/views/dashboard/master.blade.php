<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <title>Snel in huis.com</title>

    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,300,700">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/db/js/libs/css/ui-lightness/jquery-ui-1.9.2.custom.min.css">
    <link rel="stylesheet" href="/db/css/bootstrap.min.css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="/db/js/plugins/morris/morris.css">
    <link rel="stylesheet" href="/db/js/plugins/icheck/skins/minimal/blue.css">
    <link rel="stylesheet" href="/db/js/plugins/select2/select2.css">
    <link rel="stylesheet" href="/db/js/plugins/fullcalendar/fullcalendar.css">

    <!-- App CSS -->
    <link rel="stylesheet" href="/db/css/style.css">
    <link rel="stylesheet" href="/db/css/custom.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="/js/chosen/chosen.css">
    <style>
        .chosen-container {
            width:100% !important;
            min-height:150px;
        }
        .chosen-results {
            width:100%;
            margin:0 !important;

        }
        .chosen-choices {
            margin:0 !important;
            min-height:150px;
        }
    </style>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="navbar">

<div class="container">

<div class="navbar-header">

<<<<<<< HEAD
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <i class="fa fa-cogs"></i>
    </button>

    <a class="navbar-brand navbar-brand-image" href="index-2.html">
        <img src="/db/img/logo.png" alt="Site Logo">
    </a>

</div> <!-- /.navbar-header -->

<div class="navbar-collapse collapse">
=======
    <!-- Custom css -->
    <link href="css/skins/custom.css" rel="stylesheet">
>>>>>>> 4b1aadab484400d82c6fa4af83ed2cb579493986

    <meta name="viewport" content="width=device-width, initial-scale=1">


<<<<<<< HEAD
    <ul class="nav navbar-nav noticebar navbar-left">

        <li class="dropdown">
            <a href="page-notifications.html" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell"></i>
                <span class="navbar-visible-collapsed">&nbsp;Notifications&nbsp;</span>
                <span class="badge">3</span>
            </a>

            <ul class="dropdown-menu noticebar-menu" role="menu">
                <li class="nav-header">
                    <div class="pull-left">
                        Notifications
                    </div>

                    <div class="pull-right">
                        <a href="javascript:;">Mark as Read</a>
                    </div>
                </li>

                <li>
                    <a href="page-notifications.html" class="noticebar-item">
                <span class="noticebar-item-image">
                  <i class="fa fa-cloud-upload text-success"></i>
                </span>
                <span class="noticebar-item-body">
                  <strong class="noticebar-item-title">Templates Synced</strong>
                  <span class="noticebar-item-text">20 Templates have been synced to the Mashon Demo instance.</span>
                  <span class="noticebar-item-time"><i class="fa fa-clock-o"></i> 12 minutes ago</span>
                </span>
                    </a>
                </li>

                <li>
                    <a href="page-notifications.html" class="noticebar-item">
                <span class="noticebar-item-image">
                  <i class="fa fa-ban text-danger"></i>
                </span>
                <span class="noticebar-item-body">
                  <strong class="noticebar-item-title">Sync Error</strong>
                  <span class="noticebar-item-text">5 Designs have been failed to be synced to the Mashon Demo instance.</span>
                  <span class="noticebar-item-time"><i class="fa fa-clock-o"></i> 20 minutes ago</span>
                </span>
                    </a>
                </li>

                <li class="noticebar-menu-view-all">
                    <a href="page-notifications.html">View All Notifications</a>
                </li>
            </ul>
        </li>


        <li class="dropdown">
            <a href="page-notifications.html" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-envelope"></i>
                <span class="navbar-visible-collapsed">&nbsp;Messages&nbsp;</span>
            </a>

            <ul class="dropdown-menu noticebar-menu" role="menu">
                <li class="nav-header">
                    <div class="pull-left">
                        Messages
                    </div>

                    <div class="pull-right">
                        <a href="javascript:;">New Message</a>
                    </div>
                </li>

                <li>
                    <a href="page-notifications.html" class="noticebar-item">
                <span class="noticebar-item-image">
                  <img src="img/avatars/avatar-1-md.jpg" style="width: 50px" alt="">
                </span>

                <span class="noticebar-item-body">
                  <strong class="noticebar-item-title">New Message</strong>
                  <span class="noticebar-item-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</span>
                  <span class="noticebar-item-time"><i class="fa fa-clock-o"></i> 20 minutes ago</span>
                </span>
                    </a>
                </li>

                <li>
                    <a href="page-notifications.html" class="noticebar-item">
                <span class="noticebar-item-image">
                  <img src="img/avatars/avatar-2-md.jpg" style="width: 50px" alt="">
                </span>

                <span class="noticebar-item-body">
                  <strong class="noticebar-item-title">New Message</strong>
                  <span class="noticebar-item-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</span>
                  <span class="noticebar-item-time"><i class="fa fa-clock-o"></i> 5 hours ago</span>
                </span>
                    </a>
                </li>

                <li class="noticebar-menu-view-all">
                    <a href="page-notifications.html">View All Messages</a>
                </li>
            </ul>
        </li>


        <li class="dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-exclamation-triangle"></i>
                <span class="navbar-visible-collapsed">&nbsp;Alerts&nbsp;</span>
            </a>

            <ul class="dropdown-menu noticebar-menu noticebar-hoverable" role="menu">
                <li class="nav-header">
                    <div class="pull-left">
                        Alerts
                    </div>
                </li>

                <li class="noticebar-empty">
                    <h4 class="noticebar-empty-title">No alerts here.</h4>
                    <p class="noticebar-empty-text">Check out what other makers are doing on Explore!</p>
                </li>
            </ul>
        </li>

    </ul>

    <ul class="nav navbar-nav navbar-right">

        <li>
            <a href="javascript:;">About</a>
        </li>

        <li>
            <a href="javascript:;">Resources</a>
        </li>

        <li class="dropdown navbar-profile">
            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                <img src="img/avatars/avatar-1-xs.jpg" class="navbar-profile-avatar" alt="">
                <span class="navbar-profile-label">rod@rod.me &nbsp;</span>
                <i class="fa fa-caret-down"></i>
            </a>

            <ul class="dropdown-menu" role="menu">

                <li>
                    <a href="page-profile.html">
                        <i class="fa fa-user"></i>
                        &nbsp;&nbsp;My Profile
                    </a>
                </li>
=======

<body>


<div id="topbar">

    <div class="container">
        <div id="logo">
            <p>Snelinhuis.com Dashboard</p>
        </div>
    </div>


</div>

<div class="container" id="body">
>>>>>>> 4b1aadab484400d82c6fa4af83ed2cb579493986

                <li>
                    <a href="page-pricing.html">
                        <i class="fa fa-dollar"></i>
                        &nbsp;&nbsp;Plans &amp; Billing
                    </a>
                </li>

<<<<<<< HEAD
                <li>
                    <a href="page-settings.html">
                        <i class="fa fa-cogs"></i>
                        &nbsp;&nbsp;Settings
                    </a>
                </li>

                <li class="divider"></li>
=======
    <div id="sidebar">

        <div class="text-center logo">
            <img src="/images/logopng.png" alt="SnelinHuis Dashboard"/>
        </div>
>>>>>>> 4b1aadab484400d82c6fa4af83ed2cb579493986

                <li>
                    <a href="account-login.html">
                        <i class="fa fa-sign-out"></i>
                        &nbsp;&nbsp;Logout
                    </a>
                </li>

            </ul>

        </li>

    </ul>

</div> <!--/.navbar-collapse -->

</div> <!-- /.container -->

</div>




<div class="mainbar">

    <div class="container">


    <button type="button" class="btn mainbar-toggle" data-toggle="collapse" data-target=".mainbar-collapse">
        <i class="fa fa-bars"></i>
    </button>

        <div class="mainbar-collapse collapse">

        @include('partials.dashboard.menu')

<<<<<<< HEAD
        </div>

    </div>

</div>

<div class="container">

    <div class="content">

        <div class="content-container">

            <div class="content-header">
                <h2 class="content-header-title">@yield('title')</h2>
            </div>

            <div class="row">
                <div id="content">
                    <div class="col-xs-12">
                        @yield('content')

                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>


        </div>

    </div>

</div>

<footer class="footer">

    <div class="container">

        <div class="row">

            <div class="col-sm-3">

                <h4>About Theme</h4>

                <br>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>

                <hr>

                <p>&copy; 2014 Jumpstart Themes.</p>

            </div> <!-- /.col -->

            <div class="col-sm-3">

                <h4>Support</h4>

                <br>

                <ul class="icons-list">
                    <li>
                        <i class="fa fa-angle-double-right icon-li"></i>
                        <a href="javascript:;">Frequently Asked Questions</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-double-right icon-li"></i>
                        <a href="javascript:;">Ask a Question</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-double-right icon-li"></i>
                        <a href="javascript:;">Video Tutorial</a>
                    <li>
                        <i class="fa fa-angle-double-right icon-li"></i>
                        <a href="javascript:;">Feedback</a>
                    </li>
                </ul>

            </div> <!-- /.col -->

            <div class="col-sm-3">

                <h4>Legal</h4>

                <br>

                <ul class="icons-list">
                    <li>
                        <i class="fa fa-angle-double-right icon-li"></i>
                        <a href="javascript:;">License</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-double-right icon-li"></i>
                        <a href="javascript:;">Terms of Use</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-double-right icon-li"></i>
                        <a href="javascript:;">Privacy Policy</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-double-right icon-li"></i>
                        <a href="javascript:;">Security</a>
                    </li>
                </ul>

            </div> <!-- /.col -->

            <div class="col-sm-3">

                <h4>Settings</h4>

                <br>

                <ul class="icons-list">
                    <li>
                        <i class="fa fa-angle-double-right icon-li"></i>
                        <a href="javascript:;">Consectetur adipisicing</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-double-right icon-li"></i>
                        <a href="javascript:;">Eiusmod tempor </a>
                    </li>
                    <li>
                        <i class="fa fa-angle-double-right icon-li"></i>
                        <a href="javascript:;">Fugiat nulla pariatur</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-double-right icon-li"></i>
                        <a href="javascript:;">Officia deserunt</a>
                    </li>
                </ul>

            </div> <!-- /.col -->

        </div> <!-- /.row -->

    </div> <!-- /.container -->

</footer>

<script src="/db/js/libs/jquery-1.10.1.min.js"></script>
<script src="/db/js/libs/jquery-ui-1.9.2.custom.min.js"></script>
<script src="/db/js/libs/bootstrap.min.js"></script>

<!--[if lt IE 9]>
<script src="/db/js/libs/excanvas.compiled.js"></script>
<![endif]-->

<!-- Plugin JS -->
<script src="/db/js/plugins/icheck/jquery.icheck.js"></script>
<script src="/db/js/plugins/select2/select2.js"></script>
<script src="/db/js/libs/raphael-2.1.2.min.js"></script>
<script src="/db/js/plugins/morris/morris.min.js"></script>
<script src="/db/js/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="/db/js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
<script src="/db/js/plugins/fullcalendar/fullcalendar.min.js"></script>

<!-- App JS -->
<script src="/db/js/target-admin.js"></script>

<!-- Plugin JS -->
<script src="/db/js/demos/dashboard.js"></script>
<script src="/db/js/demos/calendar.js"></script>
<script src="/db/js/demos/charts/morris/area.js"></script>
<script src="/db/js/demos/charts/morris/donut.js"></script>

<script>

    function confirmDelete( url )
    {
        ok = confirm("Weet u het zeker?");
        if(ok == true)
            window.location.assign(url);
    }

</script>

@yield('javascript')



</body>
</html>

=======
        <div id="sidebar-menu">
            @include('partials.dashboard.menu')
        </div>

    </div>

        <div id="content">
            @yield('content')
        </div>

</div>
<script>

    function confirmDelete( url )
    {
        ok = confirm("Weet u het zeker?");
        if(ok == true)
            window.location.assign(url);
    }
</script>
<script type="text/javascript" src="plugins/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

@yield('javascript')

</body>

</html>
>>>>>>> 4b1aadab484400d82c6fa4af83ed2cb579493986
