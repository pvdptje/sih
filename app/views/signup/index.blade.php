@extends('master')
@section('content')

<!-- main start -->
<section class="main">

    <!-- page intro start -->
    <div class="page-intro">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="page-title">Restaurant aanmelden</h1>
                </div>
                <div class="col-md-6">
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                            <li class="active">Restaurant aanmelden</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page intro end -->

    <!-- main content wrapper start -->
    <div class="main-content-wrapper">
        <div class="container">
            <div class="row">


                <!-- main content start -->
                <section class="main-content col-md-8">

                    <h2 class="title">Gratis aanmelden</h2>

                    Nadat u uw aanvraag voltooid nemen wij deze in behandeling. Na zorgvuldige controle van uw gegevens krijgt u te horen of uw bedrijf is geaccepteerd.</p>

                    <p>Hierna kunt u via uw persoonlijke dashboard de gewenste betaalproviders aanleveren, uw bezorgtijden en producten instellen.</p>

                    <br />

                    @if(!$errors->isEmpty())



                    <div class="alert alert-danger">
                        Niet alle velden zijn correct ingevuld.
                    </div>


                    @endif

                    {{ Form::open(array('route' => 'signup.store', 'class' => 'form-horizontal', 'method' => 'post')) }}

                    <form class="form-horizontal" role="form">


                        <h4>Contactpersoon</h4>
                        <hr />
                        <div class="form-group <?php echo $errors->has('firstname') ? 'has-error' : null?>">
                            <label class="col-sm-2 control-label bold">Voornaam</label>
                            <div class="col-sm-10">
                               {{ Form::text('firstname', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="form-group <?php echo $errors->has('lastname') ? 'has-error' : null?>">
                            <label class="col-sm-2 control-label bold">Achternaam</label>
                            <div class="col-sm-10">
                                {{ Form::text('lastname', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="form-group <?php echo $errors->has('email') ? 'has-error' : null?>">
                            <label class="col-sm-2 control-label bold">Emailadres</label>
                            <div class="col-sm-10">
                                {{ Form::email('email', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="form-group  <?php echo $errors->has('phone') ? 'has-error' : null?>">
                            <label class="col-sm-2 control-label bold">Telefoon</label>
                            <div class="col-sm-10">
                                {{ Form::text('phone', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <hr />
                        <h4>Restaurant</h4>
                        <hr />
                        <div class="form-group  <?php echo $errors->has('name') ? 'has-error' : null?>">
                            <label class="col-sm-2 control-label bold">Naam</label>
                            <div class="col-sm-10">
                                {{ Form::text('name', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="form-group  <?php echo $errors->has('street') || $errors->has('housenumber') ? 'has-error' : null?>">
                            <label class="col-sm-2 control-label bold">Adres</label>
                            <div class="col-sm-10">

                                <div class="row">
                                    <div class="col-sm-8">
                                        <label>Straatnaam</label>

                                        {{ Form::text('street', null, array('class' => 'form-control')) }}
                                    </div>

                                    <div class="col-sm-4">
                                        <label>Huisnummer</label>

                                        {{ Form::text('housenumber', null, array('class' => 'form-control')) }}
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group <?php echo $errors->has('postcode') || $errors->has('city') ? 'has-error' : null?>">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">

                                <div class="row form-group <?php echo $errors->has('postcode') || $errors->has('city') ? 'has-error' : null?>">
                                    <div class="col-sm-4">
                                        <label>Postcode</label>
                                        {{ Form::text('postcode', null, array('class' => 'form-control')) }}
                                    </div>

                                    <div class="col-sm-8">
                                        <label>Plaatsnaam</label>
                                        {{ Form::text('city', null, array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group <?php echo $errors->has('cocnumber') ? 'has-error' : null?>">
                            <label class="col-sm-2 control-label  bold">KVK Nummer</label>
                            <div class="col-sm-10">
                                {{ Form::text('cocnumber', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="form-group <?php echo $errors->has('termsofservice') ? 'has-error' : null?>">
                            <label class="col-sm-2 control-label  bold">AV</label>
                            <div class="col-sm-10">

                                {{ Form::checkbox('termsofservice', 1) }} Ik heb de <a href="#ad">algemene voorwaarden</a> gelezen, en ga hiermee akkoord.
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary pull-right">Aanvraag indienen</button>
                            </div>

                        </div>
                    {{ Form::close() }}

                     </section>
                <!-- main content end -->

                <!-- sidebar start -->
                <aside class="sidebar col-md-3 col-md-offset-1">

                    <div class="block clearfix">
                        <h3>Hoe werkt de uitbetaling?</h3>
                        <p>
                            Het doel van SnelinHuis.com is te fungeren als `doorgeefluik`. Dit houd in dat u uw eigen betaalproviders moet aanleveren. Wij integreren deze vervolgens in uw account.
                        </p>
                        <p>
                            Hierdoor zit u direct aangesloten op uw eigen bank of provider, en heeft u ook direct beschikking tot uw geld. Op deze manier heeft u alles in eigen hand.
                        </p>
                        <p>

                           <a href="#"><i class="fa fa-arrow-circle-right"></i> Uitgebreide informatie betreft het aanleveren van betaalproviders.</a>
                        </p>
                    </div>


                </aside>
                <!-- sidebar end -->

            </div>
        </div>
    </div>
    <!-- main content wrapper end -->

</section>

@stop