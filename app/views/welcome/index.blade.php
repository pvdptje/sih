@extends('master')
@section('content')

<!-- banner start -->
<div class="banner">

    <div class="container" id="splash">

        <div id="postcodechecker" class="col-lg-8 col-md-8 col-sm-8 col-xs-12">

            <h2>Voer uw postcode in</h2>

            <div class="row">
                <div class="col-sm-8 col-xs-12">
                    {{ Form::text('zipcode', null, array('class' => 'form-control zipfield larger', 'placeholder' => '1234AB')) }}
                </div>
                <div class="col-sm-4 col-xs-12">
                    <button type="submit" class="btn btn-primary larger full-width" id="order-button"><i class="fa fa-arrow-circle-right"></i> Hongerr!</button>
                </div>


            </div>

            <div class="clearfix"></div>





        </div>


    </div>


</div>
<!-- banner end -->

<!-- main start -->
<section class="main">



</section>


@stop

@section('javascript')

<script>

    $( function ( ) {
        fixHeader = function()
        {
            vHeight = $("#order-button").height();
            vHeight = vHeight + 20;
            $(".zipfield").css('height', vHeight);

            offset = $(".banner").height();
            if(offset > 400)
            {
                offset = offset - 300;

                if($("#splash").width() < 800)
                {
                    offset = offset - 100;
                }
                $('#postcodechecker').css('margin-top', offset);
            } else {
                $('#postcodechecker').css('margin-top', 0);
            }
        };


        fixHeader();



        $(window).resize( function(){
            fixHeader();
        });





    });

</script>

@stop