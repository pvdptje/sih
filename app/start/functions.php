<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/12/14
 * Time: 5:42 PM
 */


if ( !function_exists ( 'pre' ))
{
    function pre( $data , $kill = true )
    {
        echo '<pre>'.print_r($data,true).'</pre>';
        if($kill)
            exit;
    }
}

if( !function_exists( 'is_subdomain') )
{
    function is_subdomain()
    {
        $exceptions = ['www.snelinhuis.com', 'snelinhuis.com'];

        $host = !empty($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';

        if(!in_array($host, $exceptions))
            return true;

        return false;
    }
}

if( !function_exists( 'format_price') )
{
    function format_price( $price )
    {
        setlocale(LC_MONETARY, 'de_DE');
        return '&euro;'.money_format('%.2n', (float) $price );
    }
}

if( !function_exists( 'format_price') )
{
    function format_price( $price )
    {
        setlocale(LC_MONETARY, 'de_DE');
        return '&euro; '.money_format('%.2n', (float) $price );
    }
}

if ( !function_exists( 'is_local') )
{
    function is_local()
    {
        return $_SERVER['SERVER_ADDR'] == '127.0.0.1';
    }
}

if( !function_exists('subdomain_link') )
{
    function subdomain_link( \App\Restaurant\Restaurant $restaurant )
    {
        return 'http://'.$restaurant->subdomain.'.snelinhuis.com';
    }
}

if( !function_exists('route_contains') )
{
    /**
     * Returns wether a route name contains
     * a given string or array with string
     * @param string/array $stringOrArray
     * @param array $routesToExclude
     * @return bool
     */
    function route_contains( $stringOrArray , $routesToExclude = array() )
    {
        $routeName = app()->make('router')->currentRouteName();

        if(in_array($routeName, $routesToExclude))
            return false;

        $containsString = function( $stringOrArray ) use ($routeName)
        {
            if(is_string($stringOrArray))
            {
                if(strstr($routeName, $stringOrArray))
                    return true;
            }

            return false;
        };
        if(is_array($stringOrArray))
        {
            foreach($stringOrArray as $string)
            {
                if($containsString($string))
                    return true;
            }
        }

        if(is_string($stringOrArray))
        {
            return $containsString($string);
        }


        return false;


    }
}
if( !function_exists( 'current_route') )
{

    function current_route()
    {
        return app()->make('router')->currentRouteName();

    }

}

if ( !function_exists( 'is_active') )
{

    function is_active( $routeName , $returnActive = true )
    {
        if(current_route() == $routeName)
        {
            if($returnActive)
                return 'active';

            return true;
        }

        return null;
    }

}