<?php

/**
 * Hackish way to prevent all other routes from triggering
 * when on a subdomain.
 */

// Quick fix ^^
if(is_subdomain())
{
    Route::group(['domain' => '{restaurant}.snelinhuis.com'], function() {

<<<<<<< HEAD
=======

Route::group( array('as' => 'testgroup'), function() {

    Route::get('/test', ['uses' => 'Test@index']);

});



>>>>>>> 4b1aadab484400d82c6fa4af83ed2cb579493986

        Route::group(['before' => 'is.valid.subdomain'], function() {
            Route::get('/', ['uses' => 'App\\Website\\Restaurant\\Controller@index', 'as' => 'website.restaurant.index']);

            Route::post('/cart/delete/{key}', ['uses' => 'App\\Website\\Cart\\Controller@delete', 'as' => 'website.cart.delete']);


            Route::get('/afrekenen', ['uses' => 'App\\Website\\Checkout\\Controller@index', 'as' => 'website.checkout.index']);
            Route::post('/afrekenen', ['uses' => 'App\\Website\\Checkout\\Controller@store', 'as' => 'website.checkout.store']);
            Route::get('/bedankt', ['uses' => 'App\\Website\\Checkout\\Controller@show', 'as' => 'website.checkout.show']);

            /**
             * @todo Rename/refactor and Add filter to check wether product belongs to restaurant
             */
            Route::any('/cart/add/{productid}', ['uses' => 'App\\Website\\Cart\\Controller@add', 'as' => 'website.cart.store']);
            Route::get('/cart/', ['uses' => 'App\\Website\\Cart\\Controller@index', 'as' => 'website.cart.index']);

            Route::get('/cart/config/{productid}', ['uses' => 'App\\Website\\Cart\\Controller@config', 'as' => 'website.cart.config']);
            Route::post('/cart/config/{productid}', ['uses' => 'App\\Website\\Cart\\Controller@store', 'as' => 'website.cart.config.store']);

        });
    });
}

    // Non subdomain related routes


    Route::get('/', ['uses' => 'App\\Website\\Front\\Controller@index', 'as' => 'website.front.index']);

    Route::post('/', ['uses' => 'App\\Website\\Front\\Controller@store', 'as' => 'website.front.store']);



    Route::get('/test', ['uses' => 'Test@index']);



    Route::get('/restaurant-aanmelden', ['uses' => 'App\\Signup\\Controller@index', 'as' => 'signup.index']);
    Route::get('/aanmelden-bedankt', ['uses' => 'App\\Signup\\Controller@show', 'as' => 'signup.thanks']);
    Route::post('/restaurant-aanmelden', ['uses' => 'App\\Signup\\Controller@store', 'as' => 'signup.store']);

Route::get('/dashboard/logout', ['uses' => 'App\\Dashboard\\Login\\Controller@logout', 'as' => 'dashboard.login.logout']);




    /**
     * If the moderator is already logged in, and on the login page
     * we will redirect them to the index
     */
    Route::group( array('before' => 'dashboard.login'), function() {

<<<<<<< HEAD
        Route::get('/dashboard/login', ['uses' => 'App\\Dashboard\\Login\\Controller@index', 'as' => 'dashboard.login.index']);
        Route::post('/dashboard/login', ['uses' => 'App\\Dashboard\\Login\\Controller@store', 'as' => 'dashboard.login.store']);
=======
    Route::group(array('before' => 'moderator.has.no.restaurant'), function() {

        Route::get('/dashboard/restaurants/f-create', ['uses' => 'App\\Dashboard\\Restaurant\\Controller@createFirst', 'as' => 'dashboard.restaurant.createFirst'] );
        Route::post('/dashboard/restaurants/f-create', ['uses' => 'App\\Dashboard\\Restaurant\\Controller@storeFirst', 'as' => 'dashboard.restaurant.storeFirst'] );
>>>>>>> 4b1aadab484400d82c6fa4af83ed2cb579493986

    });

    Route::get('/dashboard/logout', ['uses' => 'App\\Dashboard\\Login\\Controller@logout', 'as' => 'dashboard.login.logout']);


    /**
     * Checks if moderator is logged in
     */
    Route::group(array('before' => 'dashboard.auth'), function() {


        /**
         * Create the first restaurant
         */

        Route::group(array('before' => 'moderator.has.no.restaurant'), function() {

            Route::get('/dashboard/restaurants/f-create', ['uses' => 'App\\Dashboard\\Restaurant\\Controller@createFirst', 'as' => 'dashboard.restaurant.createFirst'] );
            Route::post('/dashboard/restaurants/f-create', ['uses' => 'App\\Dashboard\\Restaurant\\Controller@storeFirst', 'as' => 'dashboard.restaurant.storeFirst'] );

        });



        /**
         * these pages are only accessible if a moderator has created at least 1 restaurant
         */

        Route::group(array('before' => 'moderator.has.restaurant'), function() {


<<<<<<< HEAD


            Route::get('/dashboard/restaurants/create', ['uses' => 'App\\Dashboard\\Restaurant\\Controller@create', 'as' => 'dashboard.restaurant.create'] );
            Route::get('/dashboard/', ['uses' => 'App\\Dashboard\\Front\\Controller@index', 'as' => 'dashboard.front.index']);
            Route::get('/dashboard/restaurants', ['uses' => 'App\\Dashboard\\Restaurant\\Controller@index', 'as' => 'dashboard.restaurant.index'] );


            Route::group(array('before' => 'moderator.owns.restaurant'), function( ) {

                Route::get('/dashboard/restaurant/{id}/show', ['uses' => 'App\\Dashboard\\Restaurant\\Controller@show', 'as' => 'dashboard.restaurant.show'] );

                /**
                 * Deliveryzones
                 */
                Route::get('/dashboard/restaurant/{id}/settings/deliveryzone/index', ['uses' => 'App\\Dashboard\\Restaurant\\Settings\DeliveryZone\Controller@index', 'as' => 'dashboard.restaurant.settings.deliveryzones.index'] );
                Route::get('/dashboard/restaurant/{id}/settings/deliveryzone/getZones', ['uses' => 'App\\Dashboard\\Restaurant\\Settings\DeliveryZone\Controller@getZones', 'as' => 'dashboard.restaurant.settings.deliveryzones.getzones'] );
                Route::post('/dashboard/restaurant/{id}/settings/deliveryzone/store', ['uses' => 'App\\Dashboard\\Restaurant\\Settings\DeliveryZone\Controller@store', 'as' => 'dashboard.restaurant.settings.deliveryzones.store'] );

                /**
                 * The restaurant menu editing sstuff
                 */
                Route::get('/dashboard/restaurant/{id}/menu', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\Controller@index', 'as' => 'dashboard.restaurant.menu.index'] );
                Route::post('/dashboard/restaurant/{id}/menu/addgroup', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\Controller@addGroup', 'as' => 'dashboard.restaurant.menu.addgroup'] );
                Route::post('/dashboard/restaurant/{id}/deletegroup/{groupid}', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\Controller@deleteGroup', 'as' => 'dashboard.restaurant.menu.deletegroup'] );

                /**
                 * Create a new product
                 */
                Route::get('/dashboard/restaurant/{id}/menu/product/{groupid}/create', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductController@create', 'as' => 'dashboard.restaurant.menu.product.create'] );
                Route::post('/dashboard/restaurant/{id}/menu/product/{groupid}/create', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductController@store', 'as' => 'dashboard.restaurant.menu.product.store'] );

                /**
                 * Edit a product
                 */


                Route::any('/dashboard/restaurant/{id}/menu/product/{productid}/edit', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductController@edit', 'as' => 'dashboard.restaurant.menu.product.edit'] );
                Route::post('/dashboard/restaurant/{id}/menu/product/{productid}/update', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductController@update', 'as' => 'dashboard.restaurant.menu.product.update'] );

                /**
                 * Delete a product
                 */

                Route::get('/dashboard/restaurant/{id}/menu/product/{productid}/delete', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductController@delete', 'as' => 'dashboard.restaurant.menu.product.delete'] );


                /**
                 * Loads the menu
                 */
                Route::get('/dashboard/restaurant/{id}/menu/getmenu', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\Controller@getMenu', 'as' => 'dashboard.restaurant.menu.getmenu'] );


                /**
                 * Shows a list of product extras
                 */
                Route::get('/dashboard/restaurant/{id}/menu/productextras', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductExtrasController@index', 'as' => 'dashboard.restaurant.menu.productextras.index'] );

                /**
                 * Creation of a product extra
                 */
                Route::get('/dashboard/restaurant/{id}/menu/productextras/create', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductExtrasController@create', 'as' => 'dashboard.restaurant.menu.productextras.create'] );
                Route::post('/dashboard/restaurant/{id}/menu/productextras/store', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductExtrasController@store', 'as' => 'dashboard.restaurant.menu.productextras.store'] );



                /**
                 * Edit of a product extra
                 */
                Route::get('/dashboard/restaurant/{id}/menu/productextras/edit/{extraid}', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductExtrasController@edit', 'as' => 'dashboard.restaurant.menu.productextras.edit'] );
                Route::post('/dashboard/restaurant/{id}/menu/productextras/edit/{extraid}', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductExtrasController@update', 'as' => 'dashboard.restaurant.menu.productextras.update'] );

                /**
                 * Delete a product extra
                 */
                Route::get('/dashboard/restaurant/{id}/menu/productextras/edit/{extraid}/delete', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductExtrasController@delete', 'as' => 'dashboard.restaurant.menu.productextras.delete'] );


                /**
                 * Setup payment methods
                 */
                Route::get('/dashboard/payment/restaurant/{id}/setup', ['uses' => 'App\\Dashboard\\Payment\\Setup\\Controller@index', 'as' => 'dashboard.payment.setup.index'] );
                Route::post('/dashboard/payment/restaurant/{id}/setup', ['uses' => 'App\\Dashboard\\Payment\\Setup\\Controller@show', 'as' => 'dashboard.payment.setup.show'] );
                Route::post('/dashboard/payment/restaurant/{id}/store/{providerID}', ['uses' => 'App\\Dashboard\\Payment\\Setup\\Controller@store', 'as' => 'dashboard.payment.setup.store'] );

                /**
                 * Profile
                 */

                Route::get('/dashboard/profile/{id}', ['uses' => 'App\\Dashboard\\Restaurant\\Settings\\Profile\\Controller@index', 'as' => 'dashboard.profile.index'] );
                Route::post('/dashboard/profile/{id}', ['uses' => 'App\\Dashboard\\Restaurant\\Settings\\Profile\\Controller@store', 'as' => 'dashboard.profile.store'] );

            });
=======
            /**
             * The restaurant menu editing sstuff
             */
            Route::get('/dashboard/restaurant/{id}/menu', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\Controller@index', 'as' => 'dashboard.restaurant.menu.index'] );
            Route::post('/dashboard/restaurant/{id}/menu/addgroup', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\Controller@addGroup', 'as' => 'dashboard.restaurant.menu.addgroup'] );
            Route::post('/dashboard/restaurant/{id}/deletegroup/{groupid}', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\Controller@deleteGroup', 'as' => 'dashboard.restaurant.menu.deletegroup'] );

            /**
             * Create a new product
             */
            Route::get('/dashboard/restaurant/{id}/menu/product/{groupid}/create', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductController@create', 'as' => 'dashboard.restaurant.menu.product.create'] );
            Route::post('/dashboard/restaurant/{id}/menu/product/{groupid}/create', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductController@store', 'as' => 'dashboard.restaurant.menu.product.store'] );

            /**
             * Edit a product
             */


            Route::any('/dashboard/restaurant/{id}/menu/product/{productid}/edit', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductController@edit', 'as' => 'dashboard.restaurant.menu.product.edit'] );
            Route::post('/dashboard/restaurant/{id}/menu/product/{productid}/update', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductController@update', 'as' => 'dashboard.restaurant.menu.product.update'] );

            /**
             * Delete a product
             */

            Route::get('/dashboard/restaurant/{id}/menu/product/{productid}/delete', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductController@delete', 'as' => 'dashboard.restaurant.menu.product.delete'] );


            /**
             * Loads the menu
             */
            Route::get('/dashboard/restaurant/{id}/menu/getmenu', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\Controller@getMenu', 'as' => 'dashboard.restaurant.menu.getmenu'] );


            /**
             * Shows a list of product extras
             */
            Route::get('/dashboard/restaurant/{id}/menu/productextras', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductExtrasController@index', 'as' => 'dashboard.restaurant.menu.productextras.index'] );

            /**
             * Creation of a product extra
             */
            Route::get('/dashboard/restaurant/{id}/menu/productextras/create', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductExtrasController@create', 'as' => 'dashboard.restaurant.menu.productextras.create'] );
            Route::post('/dashboard/restaurant/{id}/menu/productextras/store', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductExtrasController@store', 'as' => 'dashboard.restaurant.menu.productextras.store'] );



            /**
             * Edit of a product extra
             */
            Route::get('/dashboard/restaurant/{id}/menu/productextras/edit/{extraid}', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductExtrasController@edit', 'as' => 'dashboard.restaurant.menu.productextras.edit'] );
            Route::post('/dashboard/restaurant/{id}/menu/productextras/edit/{extraid}', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductExtrasController@update', 'as' => 'dashboard.restaurant.menu.productextras.update'] );

            /**
             * Delete a product extra
             */
            Route::get('/dashboard/restaurant/{id}/menu/productextras/edit/{extraid}/delete', ['uses' => 'App\\Dashboard\\Restaurant\\Menu\\ProductExtrasController@delete', 'as' => 'dashboard.restaurant.menu.productextras.delete'] );


            /**
             * Setup payment methods
             */
            Route::get('/dashboard/payment/restaurant/{id}/setup', ['uses' => 'App\\Dashboard\\Payment\\Setup\\Controller@index', 'as' => 'dashboard.payment.setup.index'] );
            Route::post('/dashboard/payment/restaurant/{id}/setup', ['uses' => 'App\\Dashboard\\Payment\\Setup\\Controller@show', 'as' => 'dashboard.payment.setup.show'] );
            Route::post('/dashboard/payment/restaurant/{id}/store/{providerID}', ['uses' => 'App\\Dashboard\\Payment\\Setup\\Controller@store', 'as' => 'dashboard.payment.setup.store'] );
        });
>>>>>>> 4b1aadab484400d82c6fa4af83ed2cb579493986

        }) ;


});

