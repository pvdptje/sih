<?php


namespace App\Eventing;





use Illuminate\Events\Dispatcher;

class EventDispatcher {

    protected $event;

    public function __construct(Dispatcher $event)
    {
        $this->event = $event;
    }


    public function dispatch( $events )
    {

        foreach($events as $event)
        {
            $this->event->fire( $this->getEventName($event),  $event );
        }

    }


    public function getEventName( $event )
    {
        return str_replace('\\', '.', get_class($event));
    }



}