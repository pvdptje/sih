<?php


namespace App\Eventing;



trait EventGenerator {


    protected  $storedEvents = [];

    /**
     * Raises an event
     * @param $event
     */
    public  function raiseEvent( $event )
    {
        $this->storedEvents[] = $event;
    }


    /**
     * Clears the stored events
     * and returns them
     * @return array
     */
    public function releaseEvents()
    {
        $events = $this->storedEvents;

        $this->storedEvents = [];

        return $events;
    }


}