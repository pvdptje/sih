<?php


namespace App\Eventing;

use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;


class ServiceProvider extends IlluminateServiceProvider {


    public function register()
    {
        $listeners = $this->app['config']->get('listeners');

        foreach($listeners as $listener)
        {
            $this->app['events']->listen('App.*', $listener);
        }
    }

}
