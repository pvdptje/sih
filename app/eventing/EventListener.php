<?php


namespace App\Eventing;


use Illuminate\Foundation\Application;

class EventListener {

    public $app;

    public function __construct( Application $app )
    {
        $this->app = $app;
    }


    public function handle( $event )
    {

        if(method_exists($this, $method = $this->getMethodName($event) ))
        {
             return  call_user_func([$this, $method], $event);
        }

    }

    protected function getMethodName( $event )
    {
        $name = (new \ReflectionClass($event))->getShortName();
        return "when".$name;
    }




}