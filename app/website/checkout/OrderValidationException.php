<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 11/9/14
 * Time: 4:17 PM
 */

namespace App\Website\Checkout;


class OrderValidationException extends \Exception {

    public $validator;


    public function __construct( $validator )
    {
        $this->validator = $validator;
    }


}