<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 11/8/14
 * Time: 8:22 AM
 */


namespace App\Website\Checkout;

use App\Restaurant\Restaurant;

use App\Traits\Cart;
use Illuminate\Foundation\Application;
use Illuminate\Routing\Redirector;
use Illuminate\View\Factory;

class Controller extends \BaseController {


    use Cart;

    protected $app;

    /**
     * @var \Illuminate\View\View
     */
    protected $view;

    protected $redirect;

    function __construct(Application $app,Factory $view, Redirector $redirector)
    {
        $this->app = $app;
        $this->view = $view;
        $this->redirect = $redirector;
    }


    public function index( $subdomain )
    {
        if($this->cartIsEmpty())
        {
            return  $this->redirect->route('website.restaurant.index', array($subdomain));
        }

        $order = $this->app->make('App\\Website\\Checkout\\Order');

        $restaurant = Restaurant::getBySubdomain($subdomain);

        return $this->view->make('website.checkout.index')->with([
            'restaurant' => $restaurant,
            'profile' => $restaurant->getProfile(),
            'banks' => $order->getListOfBanks( $restaurant ),
        ]);
    }

    public function store( $subdomain )
    {
        if($this->cartIsEmpty())
        {
            return  $this->redirect->route('website.restaurant.index', array($subdomain));
        }

        return $this->app->make('App\\Website\\Checkout\\Order')->setData( \Input::all() )->place( Restaurant::getBySubdomain($subdomain) );
    }

    public function show($subdomain)
    {

        return $this->view->make('website.checkout.show');

    }

}