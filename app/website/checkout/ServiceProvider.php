<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 11/9/14
 * Time: 6:51 PM
 */

namespace App\Website\Checkout;

use Illuminate\Support\ServiceProvider as sProvider;
class ServiceProvider extends sProvider {


    public function register()
    {
        $this->app->error(function(OrderValidationException $exception){

            return \Redirect::back()->withInput()->withErrors( $exception->validator );

        });
    }

}