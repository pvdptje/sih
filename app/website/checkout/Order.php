<?php

namespace App\Website\Checkout;

use App\Payment\Qantani;
use App\Restaurant\Restaurant;
use Illuminate\Routing\Redirector;
use Illuminate\Session\SessionManager;
use Illuminate\Validation\Factory;

use App\Eventing\EventDispatcher;



class Order {


    use \App\Traits\Cart;

    protected $validationRules = [
        'firstname' => 'required',
        'lastname' => 'required',
        'postcode' => 'required',
        'housenumber' => 'required',
        'street' => 'required',
        'city' => 'required',
        'email' => 'required',
        'type' => 'required',
    ];

    protected $data = [];

    protected $validator;

    protected $redirect;

    protected $session;

    protected $dispatcher;


    static $qantani;


    public function __construct( Factory $validator , Redirector $redirector , EventDispatcher $event , SessionManager $manager )
    {
        $this->validator = $validator;
        $this->redirect = $redirector;
        $this->dispatcher = $event;
        $this->session = $manager->driver( $manager->getDefaultDriver() );
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    public function getQantaniInstance( Restaurant $restaurant  )
    {
        if(!self::$qantani)
        {
            $qantani = $restaurant->paymentProviders->first();
            $credentials = unserialize($qantani->pivot->config);
            self::$qantani = Qantani::CreateInstance($credentials['merchantID'],$credentials['merchantKey'], $credentials['merchantSecret']);
        }


        return self::$qantani;

    }

    protected function placeQantaniPayment( Restaurant $restaurant , \App\Restaurant\Order $order)
    {
        $qantani = $this->getQantaniInstance( $restaurant );


        $url = $qantani->Ideal_Execute([
            'Amount' => $order->total,
            'Currency' => 'EUR',
            'Description' => 'Bestelling '.$order->id .' '.$restaurant->name,
            'Bank' => $this->data['bank'],
            'Return' => \URL::route('website.checkout.show', array($restaurant->subdomain))
        ]);

        $this->session->put('orderID', $order->id);

        if($url)
        {
            return $this->redirect->to($url);
        }

        throw new \Exception('Error while trying to redirect to bank');
    }

    public function place( Restaurant $restaurant )
    {

        if(isset($this->data['type']) && $this->data['type'] == 'ideal')
            $this->validationRules['bank'] = 'required';

        $validator = $this->validator->make($this->data,$this->validationRules);

        if(!$validator->fails())
        {

            $order = \App\Restaurant\Order::createNew($restaurant, $this->getCart(), $this->data);

            // @todo fix this :)
            if($this->data['type'] == 'ideal')
            {
               return $this->placeQantaniPayment($restaurant,$order);
            }

            $this->dispatcher->dispatch( $order->releaseEvents() );

            return $this->redirect->route('website.checkout.show');

        }

        throw new OrderValidationException( $validator );

    }


    public function getListOfBanks( $restaurant )
    {
        $banks = $this->getQantaniInstance($restaurant)->Ideal_getBanks();

        $listOfBanks = [];
        foreach($banks as $bank)
        {
            $listOfBanks[$bank['Id']] = $bank['Name'];
        }

        return $listOfBanks;
    }


}