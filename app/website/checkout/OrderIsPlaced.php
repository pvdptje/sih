<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 11/9/14
 * Time: 4:13 PM
 */

namespace App\Website\Checkout;

class OrderIsPlaced {

    public $order;
    public $cart;
    public $restaurant;

    public function __construct($restaurant,$order, $cart)
    {
        $this->restaurant = $restaurant;
        $this->order = $order;
        $this->cart = $cart;
    }

}