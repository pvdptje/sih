<?php

namespace App\Website\Checkout;

use App\Payment\Qantani;
use App\Restaurant\Restaurant;
use Illuminate\Routing\Redirector;
use Illuminate\Session\SessionManager;
use Illuminate\Validation\Factory;

use App\Eventing\EventDispatcher;



class Order {


    use \App\Traits\Cart;

    protected $validationRules = [
        'firstname' => 'required',
        'lastname' => 'required',
        'postcode' => 'required',
        'housenumber' => 'required',
        'street' => 'required',
        'city' => 'required',
        'email' => 'required',
        'type' => 'required',
    ];

    protected $data = [];

    protected $validator;

    protected $redirect;

    protected $session;

    protected $dispatcher;


    public function __construct( Factory $validator , Redirector $redirector , EventDispatcher $event , SessionManager $manager )
    {
        $this->validator = $validator;
        $this->redirect = $redirector;
        $this->dispatcher = $event;
        $this->session = $manager->driver( $manager->getDefaultDriver() );
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    public function getQantaniInstance( Restaurant $restaurant  )
    {
        $qantani = $restaurant->paymentProviders->first();
        $credentials = unserialize($qantani->pivot->config);
        return Qantani::CreateInstance($credentials['merchantID'],$credentials['merchantKey'], $credentials['merchantSecret']);

    }

    public function place( Restaurant $restaurant )
    {

        if(isset($this->data['type']) && $this->data['type'] == 'ideal')
            $this->validationRules['bank'] = 'required';

        $validator = $this->validator->make($this->data,$this->validationRules);

        if(!$validator->fails())
        {

            $order = \App\Restaurant\Order::createNew($restaurant, $this->getCart(), $this->data);

            if($this->data['type'] == 'bank')
            {
                $qantani = $this->getQantaniInstance( $restaurant );

                $url = $qantani->Ideal_Execute([
                    'amount' => $order->total,
                    'currency' => 'EUR',
                    'description' => 'Bestelling '.$order->id .' '.$restaurant->name,
                    'bank' => $this->data['bank'],
                    'return' => \Url::route('website.restaurant.index', array($restaurant->subdomain))
                ]);

                $this->session->put('orderID', $order->id);

                if($url)
                {
                    return $this->redirect->to($url);
                }

                throw new \Exception('Error while trying to redirect to bank');

            }

            $this->dispatcher->dispatch( $order->releaseEvents() );

            return $this->redirect->route('website.checkout.thanks');


        }

        throw new OrderValidationException( $validator );

    }


}