<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 11/8/14
 * Time: 10:22 PM
 */

namespace App\Website\Cart ;

use Illuminate\Routing\UrlGenerator;
use Illuminate\View\Factory;

use App\Restaurant\Product;

use App\Restaurant\ExtraValue;
use App\Restaurant\Extra;

class Controller extends \BaseController{


    use \App\Traits\Cart;


    protected $view;

    protected $product;

    protected $url;

    public function __construct(Factory $view , Product $product, UrlGenerator $url)
    {
        $this->product = $product;
        $this->view = $view;
        $this->url = $url;

    }



    public function index($subdomain)
    {
        return $this->view->make('website.restaurant.cart')->with('cart', $this->getCart())->with('subdomain', $subdomain);
    }

    /**
     * @param $subdomain
     * @param $productid
     * @return array
     */
    public function add($subdomain, $productid)
    {
        $response = [];

        $product = $this->product->findOrFail($productid);

        if(!$product->extras->isEmpty())
        {

            $response['extra'] = true;
            $response['href'] =  $this->url->route('website.cart.config', array($subdomain, $productid));

        } else {

            $response['extra'] = false;
            $this->getCart()->add($productid, $product->name, 1, $product->price);

        }

        return $response;

    }



    public function config($subdomain, $productid)
    {
        $product = $this->product->findOrFail($productid);

        return $this->view->make('website.restaurant.addproduct')->with([
            'product' => $product,
            'subdomain' => $subdomain
        ]);
    }

    public function delete($subdomain, $key)
    {
        $this->getCart()->remove($key);
    }

    public function store($subdomain, $productid)
    {
        $input = \Input::all();
        $product = $this->product->findOrFail($productid);
        $error = false;

        foreach($product->extras as $extra)
        {
            if($extra->required)
            {
                if(!array_key_exists($extra->id, $input)){
                    $error = 'Controleer of alle verplichte keuzes zijn ingevuld';
                    break;
                }
            }
        }

        if(strlen($error))
        {
            return \Redirect::back()->with('message', $error)->withInput();
        }

        $options = [];

        $price = $product->price;
        foreach($input as $extra_id => $value_id)
        {


            $extraValue = ExtraValue::findOrFail($value_id);
            $extra = Extra::findOrFail($extra_id);

            $options[$extra->name] = $extraValue->name;


            $price = $price + $extraValue->price;

        }





        $this->getCart()->add($productid, $product->name, 1, $price, $options);


        return \Redirect::back()->with(['message' => 'Product toegevoegd', 'done' => true]);
    }


}