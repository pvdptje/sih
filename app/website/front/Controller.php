<?php



namespace App\Website\Front;


use App\Restaurant\DeliveryZone;
use App\Restaurant\OpeningHour;
use App\Restaurant\Postcode;
use App\Restaurant\Restaurant;
use Illuminate\Support\Collection;
use Illuminate\View\Factory;

class Controller extends \BaseController {


    protected $view;

    function __construct(Factory  $view)
    {
        $this->view = $view;
    }

    public function index()
    {

        $restaurants = Restaurant::activeOnly()->get();

        return $this->view->make('website.front.index')->with('restaurants', $restaurants );

    }

    public function store()
    {

        $zip = \Input::get('zipcode');
        $zipcode = Postcode::where('postcode','LIKE', '%'.substr(trim($zip),0,4).'%')->remember(0)->take(1)->get()->first();

        if(!is_object($zipcode))
            return \Redirect::back()->withInput();

        $deliveryZones = DeliveryZone::where('city', $zipcode->city)->remember(0)->get();

        $restaurants = new Collection();

        foreach($deliveryZones as $deliveryZone)
        {
            $restaurant = $deliveryZone->restaurant;
            $restaurants->push($restaurant);
        }


        return $this->view->make('website.search.index')->with('restaurants', $restaurants)->with('city', $zipcode->city);


    }





}