<?php
/**
 * Needs heavy refactoring,
 * only for demo
 */


namespace App\Website\Restaurant;


use App\Restaurant\Restaurant;
use App\Traits\Cart;


class Controller extends \BaseController{


    use Cart;

    public function index( $subdomain )
    {

        $restaurant = Restaurant::getBySubdomain($subdomain);

        $profile = $restaurant->getProfile();

        return \View::make('website.restaurant.index')->with('restaurant', $restaurant)->with('profile', $profile)->with('cart', $this->getCart())->with('subdomain', $subdomain);

    }





}