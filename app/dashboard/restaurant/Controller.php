<?php

namespace App\Dashboard\Restaurant;

use App\Payment\AvailablePaymentMethodHelper;
use App\Signup\Application;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\Factory;
use Laracasts\Commander\ValidationCommandBus;
use Ollieread\Multiauth\MultiManager;

use Laracasts\Commander\CommanderTrait;


use App\Restaurant\Restaurant;

class Controller extends \BaseController {



    use CommanderTrait;

    /**
     * @var \Illuminate\View\Factory
     */
    protected $view;

    /**
     * @var \App\Dashboard\Moderator\Moderator $moderator;
     */

    protected $moderator;


    /**
     * @var \App\Signup\Application;
     */
    protected $moderatorApplication;


    protected $request;

    protected $redirect;

    /**
     * @var \Laracasts\Commander\ValidationCommandBus
     */
    protected $commandBus;

    function __construct(Factory $view, MultiManager $manager, Application $application ,  ValidationCommandBus $commandBus, Request $request , Redirector $redirect)
    {
        $this->view = $view;
        $this->moderator = $manager->moderator()->user();
        $this->availablePaymentMethods = AvailablePaymentMethodHelper::getKeyValueList();
        $this->moderatorApplication = $application->where('email', $this->moderator->email)->get()->first();
        $this->commandBus = $commandBus;
        $this->request = $request;
        $this->redirect = $redirect;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return $this->view->make('dashboard.restaurant.index')->with('restaurants', $this->moderator->restaurants);
    }

    /**
     * This page is used for registering any but the first
     * restaurant
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->view->make('dashboard.restaurant.create');
    }

    /**
     * This page uses the application, for the `first`
     * restaurant they register
     * @return \Illuminate\View\View
     */
    public function createFirst()
    {
        return $this->view->make('dashboard.restaurant.createfirst')->with(
            array(
                'application' => $this->moderatorApplication,
                'payment_providers' =>  AvailablePaymentMethodHelper::getKeyValueList()
            )
        );
    }


    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeFirst()
    {

       $input = $this->request->all();
       extract($input);

       $this->commandBus->execute( new RegisterFirstRestaurantCommand(

           $subdomain,
           $name,
           $street,
           $housenumber,
           $postcode,
           $city,
           $cocnumber,
           $paymentProviders,
           $phonenumber,
           $toc

       ) , null, [ 'App\\Dashboard\\Restaurant\\RegisterFirstRestaurantValidator' ]);


       return $this->redirect->route('dashboard.restaurant.index');

    }


    public function show( $id )
    {

     return $this->view->make('dashboard.restaurant.show')->with('restaurant', Restaurant::find($id));

    }




}
