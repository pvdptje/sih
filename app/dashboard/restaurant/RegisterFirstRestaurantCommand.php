<?php namespace App\Dashboard\Restaurant;

class RegisterFirstRestaurantCommand {

    /**
     * @var string
     */
    public $subdomain;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $street;

    /**
     * @var string
     */
    public $housenumber;

    /**
     * @var string
     */
    public $postcode;

    /**
     * @var string
     */
    public $city;

    public $phonenumber;

    /**
     * @var string
     */
    public $cocnumber;

    /**
     * @var string
     */
    public $paymentProviders;

    /**
     * @var string
     */
    public $toc;

    /**
     * @param string subdomain
     * @param string name
     * @param string street
     * @param string housenumber
     * @param string postcode
     * @param string city
     * @param string cocnumber
     * @param string paymentProviders
     * @param string toc
     */
    public function __construct($subdomain, $name, $street, $housenumber, $postcode, $city, $cocnumber, $paymentProviders, $toc, $phonenumber)
    {
        $this->subdomain = $subdomain;
        $this->name = $name;
        $this->street = $street;
        $this->housenumber = $housenumber;
        $this->postcode = $postcode;
        $this->city = $city;
        $this->cocnumber = $cocnumber;
        $this->paymentProviders = $paymentProviders;
        $this->toc = $toc;
        $this->phonenumber = $phonenumber;
    }

}