<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/18/14
 * Time: 11:43 PM
 */

namespace App\Dashboard\Restaurant;


use App\Restaurant\Restaurant;

class FirstRestaurantIsRegistered {


    protected $restaurant;

    public function __construct( Restaurant $restaurant )
    {
        $this->restaurant = $restaurant;
    }
}