<?php

/**
 * NEEEDS MAJOR REFACTORING
 *
 * CURRENT STATE IS FOR TEH DEMO
 */

namespace App\Dashboard\Restaurant\Menu;


use App\Restaurant\Extra;
use App\Restaurant\ExtraValue;
use App\Restaurant\FoodCategories;
use App\Restaurant\Product;
use App\Restaurant\ProductExtra;
use App\Restaurant\Restaurant;
use Illuminate\View\Factory;

class Controller extends \BaseController {

    protected $view;

    function __construct(Factory $view)
    {
        $this->view = $view;
    }


    public function index( $id )
    {
        return $this->view->make('dashboard.restaurant.menu.index')->with('restaurant', Restaurant::findOrFail($id));
    }

    public function addGroup( $id )
    {

        $restaurant                     = Restaurant::findOrFail($id);
        $group                          = \Input::get('group');
        $foodCategory                   = new FoodCategories();
        $foodCategory->name             = $group;
        $foodCategory->restaurants_id   = $restaurant->id;
        $foodCategory->save();

    }

    public function deleteGroup( $restaurantID, $foodGroupId )
    {
        FoodCategories::findOrFail($foodGroupId)->delete();

    }

    public function getMenu( $id )
    {
        $restaurant                     = Restaurant::findOrFail($id);

        return $this->view->make('dashboard.restaurant.menu.menucart')->with('restaurant', $restaurant);

    }

}
