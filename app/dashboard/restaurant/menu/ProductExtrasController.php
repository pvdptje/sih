<?php

/**
 * Needs major refactoring,
 * current state is only for demo :)
 */

namespace App\Dashboard\Restaurant\Menu;


use App\Restaurant\Extra;
use App\Restaurant\ExtraValue;
use App\Restaurant\Restaurant;

class ProductExtrasController extends \BaseController {


    public function index( $id )
    {

        return \View::make('dashboard.restaurant.menu.productextras.index')->with('restaurant', Restaurant::findOrFail($id) );
    }

    public function create( $id )
    {
        return \View::make('dashboard.restaurant.menu.productextras.create')->with('restaurant', Restaurant::findOrFail($id) );
    }

    public function store( $restaurantid )
    {

        $input = \Input::all();

        $extra = new Extra();

        $extra->name = $input['name'];
        $extra->type = isset($input['type']) ? $input['type'] : 'single_choice';
        $extra->restaurant_id = $restaurantid;
        $extra->max_choices = $extra->type == 'multiple_choice' ? $input['max_choices'] : 0;
        $extra->required = isset($input['required']) ?: false;
        $extra->save();



        if(!empty($input['extravalue']))
        {
            foreach($input['extravalue']['name'] as $id => $name)
            {

                $extraValue = new ExtraValue();
                $extraValue->extras_id = $extra->id;
                $extraValue->name = $name;
                $extraValue->price = isset($input['extravalue']['price'][$id]) ? $input['extravalue']['price'][$id] : 0;
                $extraValue->save();

            }

        }





        return \Redirect::route('dashboard.restaurant.menu.productextras.index', $restaurantid)->with('message', 'Productextra is opgeslagen.');

    }

    public function edit( $restaurantid, $extraid)
    {
        return \View::make('dashboard.restaurant.menu.productextras.edit')
                ->with('restaurant', Restaurant::findOrFail($restaurantid))
                ->with('extra', Extra::findOrFail($extraid));
    }

    public function update( $restaurantid, $extraid )
    {


        $input = \Input::all();

        $extra = Extra::find($extraid);

        $extra->name = $input['name'];
        $extra->type = $input['type'];
        $extra->max_choices = $input['type'] == 'multiple_choice' ? $input['max_choices'] : 0;
        $extra->required = isset($input['required']) ?: false;
        $extra->save();

        $extraValues = ExtraValue::where('extras_id' , $extra->id)->get();
        foreach($extraValues as $extraValue)
        {
            $extraValue->delete();
        }

        if(!empty($input['extravalue']))
        {
            foreach($input['extravalue']['name'] as $id => $name)
            {

                        $extraValue = new ExtraValue();
                        $extraValue->extras_id = $extra->id;
                        $extraValue->name = $name;
                        $extraValue->price = isset($input['extravalue']['price'][$id]) ? $input['extravalue']['price'][$id] : 0;
                        $extraValue->save();

                }

         }



        return \Redirect::route('dashboard.restaurant.menu.productextras.index', $restaurantid)->with('message', 'Productextra is opgeslagen.');


    }


    public function delete($restaurantid, $extraid)
    {
        Extra::findOrFail($extraid)->delete();

        return \Redirect::route('dashboard.restaurant.menu.productextras.index', $restaurantid)->with('message', 'Productextra verwijderd.');

    }
}