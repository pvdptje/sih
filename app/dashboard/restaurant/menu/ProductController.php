<?php

/**
 * Needs major refactoring
 * only for teh demo
 */
namespace App\Dashboard\Restaurant\Menu;


use App\Restaurant\Extra;
use App\Restaurant\FoodCategories;
use App\Restaurant\Product;
use App\Restaurant\Restaurant;
class ProductController extends  \BaseController {

    public function index( $id )
    {

    }


    public function create( $id , $foodGroupId )
    {

        $restaurant = Restaurant::findOrFail($id);

        return \View::make('dashboard.restaurant.menu.product.create')->with('restaurant', $restaurant)
                                                                      ->with('foodCategoryId', $foodGroupId);

    }

    public function store($id, $foodGroupId)
    {
        $input = \Input::all();
        $restaurant = Restaurant::findOrFail($id);
        $category = FoodCategories::findOrFail($foodGroupId);

        $product = new Product();
        $product->name = $input['name'];
        $product->price = $input['price'];
        $product->food_categories_id = $category->id;
        $product->save();

        if(!empty($input['extras'])){
            $product->extras()->sync($input['extras']);
        }


    }

    public function edit($id, $productid)
    {

        $restaurant = Restaurant::findOrFail($id);

        return \View::make('dashboard.restaurant.menu.product.edit')->with('restaurant', $restaurant)
                                                                    ->with('product', Product::findOrFail($productid));


    }

    public function update($id, $productid)
    {

        $input = \Input::all();

        $product = Product::find($productid);

        $product->name = $input['name'];
        $product->price = $input['price'];

        $product->save();


        if(!empty($input['extras']))
        {
            $extras = [];
            foreach($input['extras'] as $id)
            {
                $extra = Extra::findOrFail($id);
                // check if this extra belongs to this restaurant @todo
                $extras[] = $extra->id;


            }

            $product->extras()->sync($extras);

        } else {
            $product->extras()->sync(array());
        }

    }

    public function delete($restaurantid, $productid)
    {
        Product::findOrFail($productid)->delete();

        return \Redirect::back()->with('message', 'Product verwijderd.');
    }

}