<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 11/2/14
 * Time: 12:57 AM
 */

namespace App\Dashboard\Restaurant\Settings\Profile;


use App\Restaurant\Restaurant;
use Illuminate\View\Factory;

class Controller extends \BaseController{



    protected $view;

    function __construct(Factory $view)
    {
        $this->view = $view;
    }


    public function index( $id )
    {

        $restaurant = Restaurant::findOrFail($id);

        $profile = $restaurant->getProfile();

        return $this->view->make('dashboard.restaurant.settings.profile.index')->with('profile', $profile)->with('restaurant', $restaurant);


    }


    public function store( $id )
    {
        $restaurant = Restaurant::findOrFail($id);

        $profile = $restaurant->getProfile();

        if(\Input::hasFile('logo'))
        {
            $profile->logo->set( \Input::file('logo') );
        }
        $input = \Input::all();
        $profile->email->set($input['email']);
        $profile->slogan->set($input['slogan']);




        $profile->save();

        return \Redirect::back()->with('message', 'Profiel bewerkt');

    }





} 