<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/26/14
 * Time: 5:25 PM
 */

namespace App\Dashboard\Restaurant\Settings\DeliveryZone;


use App\Dashboard\Exception\PostcodeNotFoundException;
use App\Restaurant\DeliveryZone;
use App\Restaurant\Postcode;
use App\Restaurant\Restaurant;

class Controller extends \BaseController {

    public function index( $restaurantID )
    {


       $restaurant = Restaurant::findOrFail($restaurantID);

       $postcode = Postcode::where('postcode', $restaurant->postcode)->get()->first();

       if($postcode)
       {
           return \View::make('dashboard.restaurant.settings.deliveryzone.index')->with('restaurant', $restaurant)->with('postcode', $postcode);
       }


       throw new PostcodeNotFoundException;


    }

    public function getZones( $restaurantID )
    {
        if( $postcode = \App\Restaurant\Postcode::where('postcode' , \Input::get('postcode'))->get()->first() )
        {
            $distance = 30;

            $lat = $postcode->lat;
            $lng = $postcode->lon;

            $radius = 6371;
            $maxlat = $lat + rad2deg($distance / $radius);
            $minlat = $lat - rad2deg($distance / $radius);
            $maxlng = $lng + rad2deg($distance / $radius / cos(deg2rad($lat)));
            $minlng = $lng - rad2deg($distance / $radius / cos(deg2rad($lat)));

            $limit = 10000;

            $postcode = new \App\Restaurant\Postcode();


            $zones = DeliveryZone::where('restaurant_id', $restaurantID)->get();

            $postcodes = $postcode->select(\DB::raw('distinct city'))->remember(0)->whereBetween('lat', array($minlat, $maxlat))->whereBetween('lon', array($minlng,$maxlng))->groupBy('city')->take($limit)->get();

            return [
                'zones' => $zones,
                'postcodes' => $postcodes
            ];
        }

        throw new PostcodeNotFoundException;

    }

    public function store($restaurantID)
    {

        \DB::statement('DELETE FROM delivery_zones WHERE restaurant_id =  ?',[$restaurantID]);

        $input = \Input::all();

        $insert = [];




        foreach($input['cities'] as $city)
        {
            $data['restaurant_id'] = $restaurantID;
            $data['city'] = $city;


            $insert[] = $data;


        }

        DeliveryZone::insert($insert);


    }

}
