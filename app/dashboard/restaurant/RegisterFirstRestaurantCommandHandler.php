<?php namespace App\Dashboard\Restaurant;

use App\Eventing\EventDispatcher;
use App\Restaurant\Restaurant;
use Illuminate\Routing\Redirector;
use Laracasts\Commander\CommandHandler;
use Ollieread\Multiauth\MultiManager;

class RegisterFirstRestaurantCommandHandler implements CommandHandler {



    protected $dispatcher;

    protected $moderator;

    protected $redirect;

    public function __construct(  EventDispatcher $dispatcher , MultiManager $manager , Redirector $redirector )
    {


        $this->dispatcher = $dispatcher;
        $this->moderator = $manager->moderator()->user();
        $this->redirect = $redirector;


    }

    /**
     * Handle the command.
     *
     * @param object $command
     */
    public function handle( $command)
    {


        // Save the restaurant

        $restaurant = Restaurant::createFirst([
            'subdomain' => $command->subdomain,
            'name' => $command->name,
            'address' => $command->street .' '.$command->housenumber,
            'postcode' => $command->postcode,
            'city' => $command->city,
            'phonenumber' => $command->phonenumber,
        ]);

        // Attach it to the moderator
        $this->moderator->restaurants()->sync([$restaurant->id], false);







    }

}