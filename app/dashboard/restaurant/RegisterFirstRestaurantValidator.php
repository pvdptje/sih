<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/18/14
 * Time: 5:06 PM
 */

namespace App\Dashboard\Restaurant;






use Illuminate\Validation\Factory;

class RegisterFirstRestaurantValidator{


    protected $validationRules = [

        'subdomain' => 'required',
        'name' => 'required',
        'street' => 'required',
        'housenumber' => 'required',
        'city' => 'required',
        'cocnumber' => 'required',
        'paymentProviders' => 'required',
        'toc' => 'required',
        'phonenumber' => 'required'

    ];


    protected $messages = [
        'required' => ':attribute is verplicht',
    ];

    protected $niceNames = [
        'subdomain' => 'Subdomein',
        'name' => 'Naam',
        'street' => 'Straat',
        'housenumber' => 'Housenummer',
        'city' => 'Plaats',
        'cocnumber' => 'KVK Nummer',
        'phonenumber' => 'Telefoonnummer',
        'paymentProviders' => 'Payment Provider',
        'toc' => 'Algemene voorwaarden'
    ];


    public function __construct( Factory $validator)
    {
        $this->validator = $validator;
    }


    public function validate( $command )
    {
        $validator = $this->validator->make((array) $command, $this->validationRules, $this->messages)->setAttributeNames( $this->niceNames );

        if( $validator->fails() )
            throw new RestaurantRegisterValidationException( $validator );

    }

} 