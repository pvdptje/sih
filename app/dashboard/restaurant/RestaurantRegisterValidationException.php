<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/18/14
 * Time: 5:12 PM
 */

namespace App\Dashboard\Restaurant;


class RestaurantRegisterValidationException extends \Exception{

    public  $validator;

    public function __construct( $validator )
    {
        $this->validator =  $validator;
    }

} 