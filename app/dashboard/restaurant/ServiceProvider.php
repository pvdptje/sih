<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/18/14
 * Time: 5:16 PM
 */

namespace App\Dashboard\Restaurant;


use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class ServiceProvider extends IlluminateServiceProvider{


    public function register()
    {

        $this->registerRestaurantRegisterValidationException();
    }

    /**
     * God damn thats a long method name ^^
     */
    public function registerRestaurantRegisterValidationException()
    {


        $this->app->error( function ( RestaurantRegisterValidationException $exception ) {


           return \Redirect::back()->withInput()->withErrors( $exception->validator );


        });

    }



} 