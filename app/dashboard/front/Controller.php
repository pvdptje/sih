<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/14/14
 * Time: 9:32 PM
 */

namespace App\Dashboard\Front;


use Illuminate\View\Factory;

class Controller extends \BaseController {

    protected $view;

    public function __construct( Factory $view)
    {
        $this->view = $view;
    }

    public function index()
    {

        return $this->view->make('dashboard.front.index');

    }

} 