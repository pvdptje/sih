<?php



namespace App\Dashboard\Login;

use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\View\Factory;

class Controller extends \BaseController {


    /**
     * @var \Illuminate\View\Factory
     */
    protected $view;

    protected $app;


    protected $request;

    function __construct( Factory $view , Application $app , Request $request )
    {
        $this->view = $view;
        $this->app = $app;
        $this->request = $request;
    }

    public function index()
    {
        return $this->view->make('dashboard.login.index');
    }


    public function store()
    {
        return $this->app->make('App\Dashboard\Login\Login')->login ( $this->request->all() );
    }


    public function logout()
    {
        \Auth::moderator()->logout();
<<<<<<< HEAD

=======
>>>>>>> 4b1aadab484400d82c6fa4af83ed2cb579493986
        return \Redirect::route('dashboard.login.index');
    }

}