<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/14/14
 * Time: 9:03 PM
 */


namespace App\Dashboard\Login;

use \Illuminate\Support\ServiceProvider as SP;

class ServiceProvider extends SP {

    public function register()
    {

        $this->registerLoginValidationException();

    }

    protected function registerLoginValidationException()
    {

        $this->app->error( function ( LoginValidationException $exception ) {

            return \Redirect::back()->withInput()->withErrors( $exception->validator );

        });

    }


}