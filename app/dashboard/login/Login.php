<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/14/14
 * Time: 8:14 PM
 */

namespace App\Dashboard\Login;




use App\Restaurant\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Validation\Factory;

use Ollieread\Multiauth\MultiManager;

class Login {


    /**
     * Validation rules
     * @var array
     */
    protected $validationRules = [
        'email' => 'required|email',
        'password' => 'required'
    ];



    /**
     * @var  \Ollieread\Multiauth\Guard
     */
    protected $auth;

    /**
     * @var \Illuminate\Routing\Redirector
     */
    protected $redirect;

    /**
     * @var \Illuminate\Validation\Factory
     */
    protected $validator;

    /**
     * @var \Illuminate\Routing\UrlGenerator
     */
    protected $url;

    /**
     * @param Redirector $redirector
     * @param Request $request
     * @param Factory $validator
     * @param UrlGenerator $generator
     */
    function __construct(Redirector $redirector,Factory $validator , UrlGenerator $generator, MultiManager $auth )
    {
        $this->redirect = $redirector;
        $this->url = $generator;
        $this->validator = $validator;
        $this->auth = $auth->moderator();
    }

    /**
     * @param array $input
     * @return \Illuminate\Http\RedirectResponse
     * @throws LoginValidationException
     */
    public function login(array $input )
    {
        $validator = $this->validator->make($input, $this->validationRules);

        if(!$validator->fails())
        {
            if($this->auth->attempt(  ['email' => $input['email'], 'password' => $input['password']] ) )
            {

                // Temp hack

                $restaurant = $this->auth->user()->restaurants->first();

                return  $this->redirect->to( $this->url->route('dashboard.restaurant.show', array($restaurant->id)) );

            }

            return $this->redirect->back()->withInput()->with('message', 'Onjuiste combinatie. Probeer het opnieuw.');

        }

        throw new LoginValidationException ( $validator );

    }





}