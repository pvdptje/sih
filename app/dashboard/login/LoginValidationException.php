<?php


namespace App\Dashboard\Login;


class LoginValidationException extends \Exception {


    public $validator;

    public function __construct ( $validator )
    {
        $this->validator = $validator;
    }

}