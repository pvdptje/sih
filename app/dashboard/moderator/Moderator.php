<?php

namespace App\Dashboard\Moderator;


use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

use Illuminate\Database\Eloquent\Model;

class Moderator extends Model implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'moderators';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');


    public function restaurants()
    {
        return $this->belongsToMany('App\\Restaurant\\Restaurant', 'moderators_restaurants', 'moderators_id', 'restaurants_id');
    }

}
