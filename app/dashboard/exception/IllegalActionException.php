<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/19/14
 * Time: 11:57 AM
 */

namespace App\Dashboard\Exception;


class IllegalActionException extends \Exception{

    public $user;
    public $action;

    public function __construct( $user, $action)
    {
        $this->user = $user;
        $this->action = $action;
    }

} 