<?php

namespace App\Dashboard\Payment\Setup;


use App\Payment\AvailablePaymentMethod;
use App\Restaurant\Restaurant;
use Illuminate\Foundation\Application;
use Illuminate\View\Factory;
use Ollieread\Multiauth\MultiManager;

class Controller extends \BaseController {

    protected $view;

    protected $moderator;

    protected $app;

    protected $configurator;

    public function __construct(Factory $view, MultiManager $auth, Application $app)
    {
        $this->view = $view;
        $this->moderator = $auth->moderator()->user();
        $this->app = $app;
    }


    /**
     * @return \Illuminate\View\View
     */
    public function index( $restaurantID )
    {

        $restaurant = Restaurant::find($restaurantID);
        return $this->view->make('dashboard.payment.setup.index')->with([
            'restaurant' => $restaurant,
            'paymentProviders' => $restaurant->paymentProviders
        ]);
    }

    /**
     * Shows the form for configurating
     * a payment provider
     * @param $restaurantID
     * @param $provider
     */
    public function show ( $restaurantID )
    {


        $providerID = \Input::get('providerID');

        $provider = AvailablePaymentMethod::findOrFail( $providerID );
        $restaurant = Restaurant::findOrFail($restaurantID);


        $config = (new Configurator( $this->moderator , $provider , $restaurant, $this->app ))->getForm();

        return $this->view->make($config['view'])->with('restaurant', $restaurant)->with('providerID', $providerID)->with('params', $config['params']);


    }


    public function store($restaurantID, $providerID)
    {

        $provider = AvailablePaymentMethod::findOrFail( $providerID );
        $restaurant = Restaurant::findOrFail($restaurantID);
        $input = \Input::all();

        $response  = (new Configurator( $this->moderator , $provider , $restaurant, $this->app ))->save( $input );

        return $response;

    }


}