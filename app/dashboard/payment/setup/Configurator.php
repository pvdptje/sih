<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/19/14
 * Time: 4:49 PM
 */

namespace App\Dashboard\Payment\Setup;

use Illuminate\Foundation\Application;

class Configurator {


    protected $moderator;
    /**
     * @var \App\Dashboard\Payment\Setup\Configurators\ConfiguratorInterface
     */
    protected $paymentProvider;

    protected $providerID;

    protected $restaurant;

    function __construct($moderator, $paymentProvider, $restaurant, Application $app)
    {
        $this->moderator = $moderator;
        $this->paymentProvider = $app->make($paymentProvider->configurator);
        $this->providerID = $paymentProvider->id;
        $this->restaurant = $restaurant;
    }

    public function getForm( )
    {
        return $this->paymentProvider->getForm( $this->restaurant, $this->providerID );
    }

    public function save( $params )
    {
        return $this->paymentProvider->save($params, $this->moderator, $this->restaurant, $this->providerID);
    }


}