<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/20/14
 * Time: 12:16 AM
 */

namespace App\Dashboard\Payment\Setup\Configurators;


use App\Payment\Qantani;
use App\Dashboard\Payment\Setup\ResponseCodes;

class QantaniIdeal implements ConfiguratorInterface{


    protected $errorMessage;

    public function getForm($restaurant, $providerID)
    {


        $config = array();
        $config['merchantID'] = null;
        $config['merchantSecret'] = null;
        $config['merchantKey'] = null;

        foreach($restaurant->paymentProviders as $provider)
        {
            if($providerID == $provider->id)
            {
                $config = $provider->pivot->config;
                if(is_string($config))
                {
                    $config = unserialize($config);
                }
            }
        }

        return [
            'view' => 'dashboard.payment.setup.configurators.qantani.qantani',
            'params' => $config,
        ];

    }

    /**
     * @param $params
     * @return string
     */
    public function test($params)
    {

        $merchantID = $params['merchantID'] ?: null;
        $merchantKey = $params['merchantKey'] ?: null;
        $merchantSecret = $params['merchantSecret'] ?: null;

        // Qantani thinks its fine to load a list of banks without providing api keys...
        $qantani = Qantani::CreateInstance($merchantID, $merchantKey, $merchantSecret );

        // We have no way of checking if the api key is for testing or live..
        // Well just try to get a list of methods

        $response = $qantani->GetPaymentMethods();

        if($response)
             return ResponseCodes::SUCCESS;


        $this->errorMessage = 'Incorrecte gegevens. Probeer opnieuw.';

        return ResponseCodes::FAILURE;

    }

    /**
     * @param $params
     * @param $moderator
     * @param $restaurant
     * @param $providerID
     * @return string
     */
    public function save($params, $moderator, $restaurant, $providerID)
    {
        $success = $this->test($params);

        if($success === ResponseCodes::SUCCESS)
        {
            foreach($restaurant->paymentProviders as $provider)
            {
                if($providerID == $provider->id)
                {
                    $provider->pivot->config = serialize($params);
                    $provider->pivot->active = true;
                    $provider->pivot->save();

                    return ResponseCodes::SUCCESS;
                }
            }
        }



        return $this->errorMessage;
    }
}