<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/19/14
 * Time: 4:50 PM
 */

namespace App\Dashboard\Payment\Setup\Configurators;



use App\Dashboard\Payment\Setup\ResponseCodes;

class MollieIdeal implements ConfiguratorInterface{

    protected $errorMessage;

    public function getForm($restaurant, $providerID)
    {

        $apiKey = null;

        foreach($restaurant->paymentProviders as $provider)
        {
            if($provider->id == $providerID)
            {
                $apiKey = $provider->pivot->config;
            }
        }


        return [
            'view' => 'dashboard.payment.setup.configurators.mollie.mollie',
            'params' => $apiKey
        ];


    }

    public function save( $params , $moderator, $restaurant, $providerID )
    {
        foreach($restaurant->paymentProviders as $provider)
        {
            if($provider->id == $providerID)
            {

                $success = $this->test( $params['apikey'] );

                if($success === ResponseCodes::SUCCESS)
                {

                    $provider->pivot->config = $params['apikey'];
                    $provider->pivot->active = true;
                    $provider->pivot->save();

                    return ResponseCodes::SUCCESS;

                }

            }
        }


        return $this->errorMessage;
    }

    public function test( $apiKey )
    {



        if(!is_local())
        {
            if(strstr(strtolower($apiKey),'test'))
            {
                $this->errorMessage = 'Test apikeys zijn niet toegestaan.';
                return ResponseCodes::FAILURE;
            }
        }
        try {

            $mollie = new \Mollie_API_Client();
            $mollie->setApiKey($apiKey);

        } catch ( \Mollie_API_Exception $e )
        {
            $this->errorMessage = 'Ongeldige apikey. Probeer opnieuw.';
            return ResponseCodes::FAILURE;
        }


        return ResponseCodes::SUCCESS;

    }
}