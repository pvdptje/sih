<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/19/14
 * Time: 4:50 PM
 */

namespace App\Dashboard\Payment\Setup\Configurators;


interface ConfiguratorInterface {



    public function getForm($restaurant, $providerID);

    public function test( $params );

    public function save( $params , $moderator, $restaurant, $providerID );

} 