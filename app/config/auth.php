<?php

return array(

    'multi' => array(
        'moderator' => array(
            'driver' => 'eloquent',
            'model' => 'App\\Dashboard\\Moderator\\Moderator'
        ),
    /*    'customer' => array(
            'driver' => 'database',
            'table' => 'users'
        )*/
    ),


	'reminder' => array(

		'email' => 'emails.auth.reminder',

		'table' => 'password_reminders',

		'expire' => 60,

	),

);
