<?php


return array(

    'namespace' => 'App\\Restaurant\\Profile\\Property\\',

    'properties' => array(
        'Address' => 'Adres gegevens',
        'Logo' => 'Logo van uw restaurant',
        'Phone' => 'Telefoonnummer',
        'Email' => 'Emailadres',
        'Slogan' => 'Slogan',
    )
);
