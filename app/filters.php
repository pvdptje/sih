<?php


//if(isset($_SERVER['REMOTE_ADDR']) && in_array($_SERVER['REMOTE_ADDR'], ['109.109.112.220', '83.128.238.209'])) die('No Access');

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/



Route::filter('is.valid.subdomain', function ( $router ){

    $subdomain = $router->getParameter('restaurant');

    $restaurant = \App\Restaurant\Restaurant::where('subdomain', $subdomain)->get()->first();

    if(empty($restaurant))
       App::abort(404);

});

Route::filter('dashboard.auth', function()
{
	if (!Auth::moderator()->check())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::route('dashboard.login.index');
		}
	}
});


Route::filter('moderator.owns.restaurant', function( $router  ) {

    $restaurant = \App\Restaurant\Restaurant::find( $router->getParameter('id') );

    $valid = false;

    if($restaurant)
    {

        foreach($restaurant->moderators as $moderator)
        {
            if(Auth::moderator()->user()->id == $moderator->id)
                $valid = true;
        }
    }

    if(!$valid)
        throw new \App\Dashboard\Exception\IllegalActionException(Auth::moderator()->user(), 'Is not the owner of this restaurant');

});

/**
 * checks if a moderator has a restaurant
 * otherwise redirects them to the creation page
 */
Route::filter('moderator.has.restaurant', function() {



    if(Auth::moderator()->user()->restaurants->isEmpty())
    {
        return Redirect::route('dashboard.restaurant.createFirst');
    }


});

/**
 * checks if the moderator has a restaurant,
 * if so redirects them to the general creating page
 */
Route::filter('moderator.has.no.restaurant', function() {


    if(Auth::moderator()->user()->restaurants->isEmpty())
    {
        return Redirect::route('dashboard.restaurant.create');
    }

});

/**
 * Check thats applied on the login page, if logged in sends them to
 * the dashboard index
 */
Route::filter('dashboard.login', function()
{
    if(Auth::moderator()->check())
    {
        return Redirect::route('dashboard.restaurant.show', array(Auth::moderator()->user()->restaurants()->first()->id));
    }
});


/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
