<?php


namespace App\Signup;


use Illuminate\Routing\Redirector;
use Illuminate\Validation\Factory;

use App\Eventing\EventDispatcher;

class SignUp {

    protected $validationRules = [
        'firstname' => 'required',
        'lastname' => 'required',
        'email' => 'required|email|unique:application',
        'phone' => 'required',
        'name' => 'required',
        'street' => 'required',
        'housenumber' => 'required',
        'postcode' => 'required',
        'cocnumber' => 'required',
        'city' => 'required',
        'termsofservice' => 'required'
    ];

    protected $data = [];

    protected $validator;

    protected $redirect;

    protected $dispatcher;

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }


    public function __construct( Factory $validator , Redirector $redirector , EventDispatcher $event )
    {
        $this->validator = $validator;
        $this->redirect = $redirector;
        $this->dispatcher = $event;
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws SignupValidationException
     */
    public function signUp()
    {
        $validator = $this->validator->make($this->data,$this->validationRules);

        if(!$validator->fails())
        {
            extract($this->data);

            $application = Application::createNew($firstname, $lastname, $email, $phone, $name, $street, $housenumber, $postcode, $cocnumber);

            $this->dispatcher->dispatch( $application->releaseEvents() );

            return $this->redirect->route('signup.thanks');

        }

        throw new SignupValidationException( $validator );

    }

}
