<?php


namespace App\Signup;

use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;


class ServiceProvider extends IlluminateServiceProvider {


    public function register()
    {
        $this->registerSignupValidationException();
    }

    public function registerSignupValidationException()
    {

        $this->app->error(function( SignupValidationException $exception ){

            return \Redirect::route('signup.index')->withInput()->withErrors($exception->validator->messages());

        });
    }


}
