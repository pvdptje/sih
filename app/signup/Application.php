<?php

namespace App\Signup;

use App\Eventing\EventGenerator;

use Illuminate\Database\Eloquent\Model;

class Application extends Model {


    use EventGenerator;

    public $timestamps = false;

    protected $table = 'application';

    static $unguarded = true;


    public static function createNew( $firstname, $lastname, $email, $phone, $name, $street, $housenumber, $postcode, $cocnumber )
    {

        $application = static::create([
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'phone' => $phone,
            'name' => $name,
            'street' => $street,
            'housenumber' => $housenumber,
            'postcode' => $postcode,
            'cocnumber' => $cocnumber,
        ]);

        $application->raiseEvent( new ApplicationSucceeded( $application ));

        return $application;

    }



}