<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/12/14
 * Time: 5:36 PM
 */

namespace App\Signup;


class SignupSuccessException extends \Exception{

    protected $data;

    public function __construct( $data )
    {
        $this->data = $data;
    }

} 