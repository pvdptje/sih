<?php


namespace App\Signup;


use Illuminate\Http\Request;
use Illuminate\View\Factory;


class Controller extends \BaseController {


    protected $view;

    public function __construct( Factory $view , Request $request, SignUp $signup )
    {
        $this->view = $view;
        $this->request = $request;
        $this->signUp = $signup;
    }

    public function index()
    {
        return $this->view->make('signup.index');
    }


    public function store()
    {
        $this->signUp->setData( $this->request->all() );
        $this->signUp->signUp();
    }


    public function show()
    {
        return $this->view->make('signup.thanks');
    }

}