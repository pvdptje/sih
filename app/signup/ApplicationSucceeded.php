<?php


namespace App\Signup;

class ApplicationSucceeded {

    /**
     * @var Application
     */
    protected $application;

    /**
     * @param \App\Signup\Application $application
     */
    public function setApplication($application)
    {
        $this->application = $application;
    }

    /**
     * @return \App\Signup\Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    public function __construct( Application $application )
    {
        $this->application = $application;
    }

}