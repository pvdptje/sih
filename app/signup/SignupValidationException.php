<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/12/14
 * Time: 5:21 PM
 */

namespace App\Signup;


class SignupValidationException extends \Exception {

    public $validator;

    public function __construct( $validator )
    {
        $this->validator = $validator;
    }

} 