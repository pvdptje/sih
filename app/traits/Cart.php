<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 11/8/14
 * Time: 8:24 AM
 */



namespace App\Traits;


trait Cart {

    /**
     * @return \Gloudemans\Shoppingcart\Cart
     */
    public function getCart()
    {

        return \Cart::instance('c');

    }


    public function cartIsEmpty()
    {
        return $this->getCart()->count() == 0;
    }

}