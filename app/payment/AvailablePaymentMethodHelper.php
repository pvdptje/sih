<?php
/**
 * Created by PhpStorm.
 * User: KillerBanana
 * Date: 10/18/14
 * Time: 11:29 PM
 */

namespace App\Payment;


class AvailablePaymentMethodHelper extends AvailablePaymentMethod{


    public static function getKeyValueList()
    {
        $list = [];
        foreach(static::all() as $paymentMethod)
        {
            $list[$paymentMethod->id] = $paymentMethod->name;
        }
        return $list;
    }


} 