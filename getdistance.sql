DELIMITER $$

CREATE FUNCTION GetDistance(orgLat DECIMAL(10,6),
                orgLong DECIMAL(10,6),
                destLat DECIMAL(10,6),
               destLong DECIMAL(10,6))
RETURNS INT(5)
BEGIN
    DECLARE dist INT(11);
  SET dist := ROUND(6371 *
        acos(cos(radians(orgLat) ) *
        cos(radians(destLat)) *
        cos(radians(destLong) - radians(orgLong)) + sin(radians(orgLat))
        * sin(radians(destLat))));
    RETURN dist;
END$$

DELIMITER ;